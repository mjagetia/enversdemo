if (typeof jQuery !== 'undefined') {
    function startIntro(){
        var intro = introJs();
          intro.setOptions({
              steps: [
                      {
                        element: '#top-nav',
                        intro: "<b>Header</b> - Showing Application, Main Navigation and Signoff.",
                        position: 'bottom'
                      },
                      {
                        element: '.brand',
                        intro: "RWO Admin - Navigate to Home [<b>H</b>]",
                        position: 'bottom'
                      },
                      {
                        element: '.intro-nav-menu-top',
                        intro: "<b>Main navigation</b> will be availble all the time as long as you are loged-in, Jump to any of these pages from anywhere, in tablet or mobile or smaller browser window it will collapse and adapt according to space.",
                        position: 'bottom'
                      },
                      {
                          element: '.intro-page-common-functions-top',
                          intro: "<b>Function menu</b> - This is the list of functions or actions available for this screen for the selected item under Main navigation, this will allow you to list, view, review, create the item you selected from the Main navigation menu.",
                          position: 'left'
                        },
                      {
                          element: '.intro-entity-list-button',
                          intro: "List the data under selected menu item",
                          position: 'left'
                        },
                        {
                          element: '.intro-entity-revisions-button',
                          intro: 'See the history of changes for selected menu item',
                          position: 'left'
                        },
                        {
                          element: '.intro-entity-create-button',
                          intro: "Create new data under selected menu item",
                          position: 'left'
                        },
                        {
                            element: '.page-header h1',
                            intro: "Screen title - This will help you in understanding what this screen will present.",
                            position: 'right'
                          },
                          {
                            element: '.intro-page-main-data',
                            intro: "This will present the relevant data or information for the Action that you have selected from the function menu.",
                            position: 'top'
                          },                      
                      {
                        element: '.intro-page-main-metadata',
                        intro: "Data Headings can help you understand what each column is for",
                        position: 'top'
                      },
                      {
                          element: '.intro-page-main-metadata th:first-child a',
                          intro: "Columns can be sorted in ascending and descending order by clicking on column heading",
                          position: 'right'
                      },
                      {
                   element: '.intro-entity-button-functions',
                        intro: "Operation for each Data entry (if any availble) are at the end of the data entry",
                        position: 'left'
                      },
                      {
                        element: '.intro-entity-button-show',
                        intro: 'Each Data entry can be seen in full detail in detail view by clicking show button.',
                        position: 'left'
                      },
                      {
                        element: '.intro-entity-button-edit',
                        intro: 'Data allowed to modify or delete can be updated or deleted via Edit button',
                        position: 'left'
                      },
//                      {
//                        element: '.intro-entity-button-delete',
//                        intro: 'Delete button is part of Edit now.',
//                        position: 'left'
//                      },
                      {
                        element: '.pagination ul',
                        intro: 'Pagination chain will be displayed if more data is available',
                        position: 'top'           
                      },
                      {
                          element: '.intro-nav-auth-top',
                          intro: 'Make sure to Logout once done !',
                          position: 'left'           
                        },
//                        {
//                            element: '#top-nav',
//                            intro: "Dont forget to see shortcuts by typing ? anywhere on the page",
//                            position: 'center'
//                          },
                      {
                        element: '.intro-testdrive-button',
                        intro: 'Feel Free to Test Drive later if needed :)',
                        position: 'bottom'
                      }
                    ]
          });

          intro.start();
      }
    
    function bindDocumentShortcuts(){
//    	$(document).bind('keydown', 'h', goToHome);
//    	$(document).bind('keydown', 'c', goToCreateItem);
//    	$(document).bind('keydown', 'l', goToListItems);
//    	$(document).bind('keydown', 'r', goToRevisions);
//    	$(document).bind('keydown', '?', showKeyboardShortcutHelp);
    	Mousetrap.bind('h', function() { goToHome(); });
    	Mousetrap.bind('c', function() { goToCreateItem(); });
    	Mousetrap.bind('l', function() { goToListItems(); });
    	Mousetrap.bind('r', function() { goToRevisions(); });
    	Mousetrap.bind('?', function() { showKeyboardShortcutHelp(); });
    }
    
    function goToHome(){
    	window.location = "/"
    }
    
    function goToCreateItem(){
    	window.location = window.location + "/create"
    }
    
    function goToListItems(){
    	window.location = window.location + "/list"
    }
    
    function goToRevisions(){
    	window.location = window.location + "/revisions"
    }
    
    
    function showKeyboardShortcutHelp(){
	    $('#keyboard-shortcuts').modal('show');
    }
	
	(function($) {
		$.sessionTimeout({
			 warnAfter: 1500000,
		     redirAfter: 180000,
		     redirUrl: '/logout',
		     logoutUrl: '/logout',
		     keepAliveUrl: '/keep-alive',
		     message: 'Your session is about to expire, if you are not working sign out'
		});
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
		$("#intro-testdrive").click(function() {
			startIntro();
			$( document ).tooltip();
		});
		if (typeof startTestDrive !== 'undefined' && startTestDrive){
			startIntro();
		}
		bindDocumentShortcuts();
		$('input, textarea').placeholder();

	})(jQuery);
}
