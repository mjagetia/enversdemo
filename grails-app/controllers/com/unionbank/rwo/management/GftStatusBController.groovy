package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class GftStatusBController {

	def gftStatusBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def gftStatusBRevisionInstanceTotal = GftStatusB.findAllRevisions().size()
		def audited = GftStatusB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [gftStatusBRevisionInstanceList: GftStatusB.findAllRevisions(params).reverse(), gftStatusBRevisionInstanceTotal: gftStatusBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = GftStatusB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [gftStatusBInstanceList: GftStatusB.list(params), gftStatusBInstanceTotal: GftStatusB.count(), audited: audited]
    }

    def create() {
		def audited = GftStatusB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [gftStatusBInstance: gftStatusBService.create(params), audited: audited]
    }

    def save() {
		def gftStatusBInstance
		try{
			gftStatusBInstance = gftStatusBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [gftStatusBInstance: gftStatusBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), gftStatusBInstance.id])
		redirect(action: "show", id: gftStatusBInstance.id)
    }

    def show(java.lang.String id) {
		def gftStatusBInstance = GftStatusB.get(id)
		if (!gftStatusBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), id])
			redirect(action: "list")
			return
		}
		def audited = GftStatusB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[gftStatusBInstance: gftStatusBInstance, audited: audited]
    }

    def edit(java.lang.String id) {
		def gftStatusBInstance
		try{
			gftStatusBInstance = gftStatusBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), id])
			redirect(action: "list")
			return
		}
		def audited = GftStatusB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[gftStatusBInstance: gftStatusBInstance, audited: audited]
    }

    def update(java.lang.String id, Long version) {
		def gftStatusBInstance
		try{
			gftStatusBInstance = gftStatusBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			gftStatusBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'gftStatusB.label', default: 'GftStatusB')] as Object[],
						"Another user has updated this GftStatusB while you were editing")
			  render(view: "edit", model: [gftStatusBInstance: gftStatusBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [gftStatusBInstance: gftStatusBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), gftStatusBInstance.id])
		redirect(action: "show", id: gftStatusBInstance.id)
    }

    def delete(java.lang.String id) {
		try{
			def gftStatusBInstance = gftStatusBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def gftStatusBInstance = new GftStatusB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'gftStatusB.label', default: 'GftStatusB'), gftStatusBInstance.id])
		render(view: "create", model: [gftStatusBInstance: gftStatusBInstance])
		return
	}
}
