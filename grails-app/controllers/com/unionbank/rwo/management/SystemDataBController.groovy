package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class SystemDataBController {

	def systemDataBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def systemDataBRevisionInstanceTotal = SystemDataB.findAllRevisions().size()
		def audited = SystemDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [systemDataBRevisionInstanceList: SystemDataB.findAllRevisions(params).reverse(), systemDataBRevisionInstanceTotal: systemDataBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = SystemDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [systemDataBInstanceList: SystemDataB.list(params), systemDataBInstanceTotal: SystemDataB.count(), audited: audited]
    }

    def create() {
		def audited = SystemDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [systemDataBInstance: systemDataBService.create(params), audited: audited]
    }

    def save() {
		def systemDataBInstance
		try{
			systemDataBInstance = systemDataBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [systemDataBInstance: systemDataBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), systemDataBInstance.id])
		redirect(action: "show", id: systemDataBInstance.id)
    }

    def show(java.lang.String id) {
		def systemDataBInstance = SystemDataB.get(id)
		if (!systemDataBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), id])
			redirect(action: "list")
			return
		}
		def audited = SystemDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[systemDataBInstance: systemDataBInstance, audited: audited]
    }

    def edit(java.lang.String id) {
		def systemDataBInstance
		try{
			systemDataBInstance = systemDataBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), id])
			redirect(action: "list")
			return
		}
		def audited = SystemDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[systemDataBInstance: systemDataBInstance, audited: audited]
    }

    def update(java.lang.String id, Long version) {
		def systemDataBInstance
		try{
			systemDataBInstance = systemDataBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			systemDataBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'systemDataB.label', default: 'SystemDataB')] as Object[],
						"Another user has updated this SystemDataB while you were editing")
			  render(view: "edit", model: [systemDataBInstance: systemDataBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [systemDataBInstance: systemDataBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), systemDataBInstance.id])
		redirect(action: "show", id: systemDataBInstance.id)
    }

    def delete(java.lang.String id) {
		try{
			def systemDataBInstance = systemDataBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def systemDataBInstance = new SystemDataB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'systemDataB.label', default: 'SystemDataB'), systemDataBInstance.id])
		render(view: "create", model: [systemDataBInstance: systemDataBInstance])
		return
	}
}
