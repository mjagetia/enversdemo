package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class PricingBController {

	def pricingBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def pricingBRevisionInstanceTotal = PricingB.findAllRevisions().size()
		def audited = PricingB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [pricingBRevisionInstanceList: PricingB.findAllRevisions(params).reverse(), pricingBRevisionInstanceTotal: pricingBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = PricingB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [pricingBInstanceList: PricingB.list(params), pricingBInstanceTotal: PricingB.count(), audited: audited]
    }

    def create() {
		def audited = PricingB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [pricingBInstance: pricingBService.create(params), audited: audited]
    }

    def save() {
		def pricingBInstance
		try{
			pricingBInstance = pricingBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [pricingBInstance: pricingBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'pricingB.label', default: 'PricingB'), pricingBInstance.id])
		redirect(action: "show", id: pricingBInstance.id)
    }

    def show(java.lang.String id) {
		def pricingBInstance = PricingB.get(id)
		if (!pricingBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'pricingB.label', default: 'PricingB'), id])
			redirect(action: "list")
			return
		}
		def audited = PricingB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[pricingBInstance: pricingBInstance, audited: audited]
    }

    def edit(java.lang.String id) {
		def pricingBInstance
		try{
			pricingBInstance = pricingBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'pricingB.label', default: 'PricingB'), id])
			redirect(action: "list")
			return
		}
		def audited = PricingB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[pricingBInstance: pricingBInstance, audited: audited]
    }

    def update(java.lang.String id, Long version) {
		def pricingBInstance
		try{
			pricingBInstance = pricingBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'pricingB.label', default: 'PricingB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			pricingBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'pricingB.label', default: 'PricingB')] as Object[],
						"Another user has updated this PricingB while you were editing")
			  render(view: "edit", model: [pricingBInstance: pricingBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [pricingBInstance: pricingBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'pricingB.label', default: 'PricingB'), pricingBInstance.id])
		redirect(action: "show", id: pricingBInstance.id)
    }

    def delete(java.lang.String id) {
		try{
			def pricingBInstance = pricingBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'pricingB.label', default: 'PricingB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'pricingB.label', default: 'PricingB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'pricingB.label', default: 'PricingB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def pricingBInstance = new PricingB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'pricingB.label', default: 'PricingB'), pricingBInstance.id])
		render(view: "create", model: [pricingBInstance: pricingBInstance])
		return
	}
}
