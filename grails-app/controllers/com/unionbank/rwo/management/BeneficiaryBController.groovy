package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class BeneficiaryBController {

	def beneficiaryBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def beneficiaryBRevisionInstanceTotal = BeneficiaryB.findAllRevisions().size()
		def audited = BeneficiaryB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [beneficiaryBRevisionInstanceList: BeneficiaryB.findAllRevisions(params).reverse(), beneficiaryBRevisionInstanceTotal: beneficiaryBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = BeneficiaryB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [beneficiaryBInstanceList: BeneficiaryB.list(params), beneficiaryBInstanceTotal: BeneficiaryB.count(), audited: audited]
    }
	
	def show(java.lang.Long id) {
		def beneficiaryBInstance = BeneficiaryB.get(id)
		if (!beneficiaryBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "list")
			return
		}
		def audited = BeneficiaryB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[beneficiaryBInstance: beneficiaryBInstance, audited: audited]
	}

/*
    def create() {
		def audited = BeneficiaryB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [beneficiaryBInstance: beneficiaryBService.create(params), audited: audited]
    }

    def save() {
		def beneficiaryBInstance
		try{
			beneficiaryBInstance = beneficiaryBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [beneficiaryBInstance: beneficiaryBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), beneficiaryBInstance.id])
		redirect(action: "show", id: beneficiaryBInstance.id)
    }

    def show(java.lang.Long id) {
		def beneficiaryBInstance = BeneficiaryB.get(id)
		if (!beneficiaryBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "list")
			return
		}
		def audited = BeneficiaryB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[beneficiaryBInstance: beneficiaryBInstance, audited: audited]
    }

    def edit(java.lang.Long id) {
		def beneficiaryBInstance
		try{
			beneficiaryBInstance = beneficiaryBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "list")
			return
		}
		def audited = BeneficiaryB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[beneficiaryBInstance: beneficiaryBInstance, audited: audited]
    }

    def update(java.lang.Long id, Long version) {
		def beneficiaryBInstance
		try{
			beneficiaryBInstance = beneficiaryBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			beneficiaryBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'beneficiaryB.label', default: 'BeneficiaryB')] as Object[],
						"Another user has updated this BeneficiaryB while you were editing")
			  render(view: "edit", model: [beneficiaryBInstance: beneficiaryBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [beneficiaryBInstance: beneficiaryBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), beneficiaryBInstance.id])
		redirect(action: "show", id: beneficiaryBInstance.id)
    }

    def delete(java.lang.Long id) {
		try{
			def beneficiaryBInstance = beneficiaryBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def beneficiaryBInstance = new BeneficiaryB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'beneficiaryB.label', default: 'BeneficiaryB'), beneficiaryBInstance.id])
		render(view: "create", model: [beneficiaryBInstance: beneficiaryBInstance])
		return
	}
*/
}
