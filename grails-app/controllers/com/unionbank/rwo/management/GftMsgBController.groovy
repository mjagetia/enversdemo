package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class GftMsgBController {

	def gftMsgBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def gftMsgBRevisionInstanceTotal = GftMsgB.findAllRevisions().size()
		def audited = GftMsgB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [gftMsgBRevisionInstanceList: GftMsgB.findAllRevisions(params).reverse(), gftMsgBRevisionInstanceTotal: gftMsgBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = GftMsgB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [gftMsgBInstanceList: GftMsgB.list(params), gftMsgBInstanceTotal: GftMsgB.count(), audited: audited]
    }
	
	def show(java.lang.Long id) {
		def gftMsgBInstance = GftMsgB.get(id)
		if (!gftMsgBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "list")
			return
		}
		def audited = GftMsgB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[gftMsgBInstance: gftMsgBInstance, audited: audited]
	}
/*
    def create() {
		def audited = GftMsgB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [gftMsgBInstance: gftMsgBService.create(params), audited: audited]
    }

    def save() {
		def gftMsgBInstance
		try{
			gftMsgBInstance = gftMsgBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [gftMsgBInstance: gftMsgBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), gftMsgBInstance.id])
		redirect(action: "show", id: gftMsgBInstance.id)
    }

    def show(java.lang.Long id) {
		def gftMsgBInstance = GftMsgB.get(id)
		if (!gftMsgBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "list")
			return
		}
		def audited = GftMsgB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[gftMsgBInstance: gftMsgBInstance, audited: audited]
    }

    def edit(java.lang.Long id) {
		def gftMsgBInstance
		try{
			gftMsgBInstance = gftMsgBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "list")
			return
		}
		def audited = GftMsgB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[gftMsgBInstance: gftMsgBInstance, audited: audited]
    }

    def update(java.lang.Long id, Long version) {
		def gftMsgBInstance
		try{
			gftMsgBInstance = gftMsgBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			gftMsgBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'gftMsgB.label', default: 'GftMsgB')] as Object[],
						"Another user has updated this GftMsgB while you were editing")
			  render(view: "edit", model: [gftMsgBInstance: gftMsgBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [gftMsgBInstance: gftMsgBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), gftMsgBInstance.id])
		redirect(action: "show", id: gftMsgBInstance.id)
    }

    def delete(java.lang.Long id) {
		try{
			def gftMsgBInstance = gftMsgBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def gftMsgBInstance = new GftMsgB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'gftMsgB.label', default: 'GftMsgB'), gftMsgBInstance.id])
		render(view: "create", model: [gftMsgBInstance: gftMsgBInstance])
		return
	}
*/
}
