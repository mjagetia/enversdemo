package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class BankInfoBController {

	def bankInfoBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def bankInfoBRevisionInstanceTotal = BankInfoB.findAllRevisions().size()
		def audited = BankInfoB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [bankInfoBRevisionInstanceList: BankInfoB.findAllRevisions(params).reverse(), bankInfoBRevisionInstanceTotal: bankInfoBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = BankInfoB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [bankInfoBInstanceList: BankInfoB.list(params), bankInfoBInstanceTotal: BankInfoB.count(), audited: audited]
    }
	
	def show(Long id) {
		def bankInfoBInstance = BankInfoB.get(id)
		if (!bankInfoBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), id])
			redirect(action: "list")
			return
		}
		def audited = BankInfoB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[bankInfoBInstance: bankInfoBInstance, audited: audited]
	}
/*
    def create() {
		def audited = BankInfoB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [bankInfoBInstance: bankInfoBService.create(params), audited: audited]
    }

    def save() {
		def bankInfoBInstance
		try{
			bankInfoBInstance = bankInfoBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [bankInfoBInstance: bankInfoBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), bankInfoBInstance.id])
		redirect(action: "show", id: bankInfoBInstance.id)
    }

    def edit(java.lang.Long id) {
		def bankInfoBInstance
		try{
			bankInfoBInstance = bankInfoBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), id])
			redirect(action: "list")
			return
		}
		def audited = BankInfoB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[bankInfoBInstance: bankInfoBInstance, audited: audited]
    }

    def update(java.lang.Long id, Long version) {
		def bankInfoBInstance
		try{
			bankInfoBInstance = bankInfoBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			bankInfoBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'bankInfoB.label', default: 'BankInfoB')] as Object[],
						"Another user has updated this BankInfoB while you were editing")
			  render(view: "edit", model: [bankInfoBInstance: bankInfoBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [bankInfoBInstance: bankInfoBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), bankInfoBInstance.id])
		redirect(action: "show", id: bankInfoBInstance.id)
    }

    def delete(java.lang.Long id) {
		try{
			def bankInfoBInstance = bankInfoBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def bankInfoBInstance = new BankInfoB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'bankInfoB.label', default: 'BankInfoB'), bankInfoBInstance.id])
		render(view: "create", model: [bankInfoBInstance: bankInfoBInstance])
		return
	}
*/
}
