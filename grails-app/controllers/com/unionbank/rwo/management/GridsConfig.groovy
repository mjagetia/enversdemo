package com.unionbank.rwo.management


import com.unionbank.rwo.management.SystemDataB

class GridsConfig {

    static grids = {
 		systemDataBDatatablesGrid  {
		dataSourceType 'gorm'
		domainClass SystemDataB
		gridImpl 'dataTables'
		fixedColumns true
		columns {
			keyName{
				filterFieldType "text"
			}
			keyValue
			description{
				enableFilter false
			}
		}
 		}
    }
}