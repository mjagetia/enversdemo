package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])

class WeeklyLimitBController {

	def weeklyLimitBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def weeklyLimitBRevisionInstanceTotal = WeeklyLimitB.findAllRevisions().size()
		def audited = WeeklyLimitB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [weeklyLimitBRevisionInstanceList: WeeklyLimitB.findAllRevisions(params).reverse(), weeklyLimitBRevisionInstanceTotal: weeklyLimitBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = WeeklyLimitB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [weeklyLimitBInstanceList: WeeklyLimitB.list(params), weeklyLimitBInstanceTotal: WeeklyLimitB.count(), audited: audited]
    }

    def create() {
		def audited = WeeklyLimitB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [weeklyLimitBInstance: weeklyLimitBService.create(params), audited: audited]
    }

    def save() {
		def weeklyLimitBInstance
		try{
			weeklyLimitBInstance = weeklyLimitBService.save(params)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.created.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB')])
			flash.messageType = 'error'
			render(view: "create", model: [weeklyLimitBInstance: new WeeklyLimitB(params)])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), weeklyLimitBInstance.id])
		flash.messageType = 'info'
		redirect(action: "show", id: weeklyLimitBInstance.id)
    }

    def show(java.lang.String id) {
		def weeklyLimitBInstance = WeeklyLimitB.get(id)
		if (!weeklyLimitBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), id])
			flash.messageType = 'error'
			redirect(action: "list")
			return
		}
		def audited = WeeklyLimitB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[weeklyLimitBInstance: weeklyLimitBInstance, audited: audited]
    }

    def edit(java.lang.String id) {
		def weeklyLimitBInstance
		try{
			weeklyLimitBInstance = weeklyLimitBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), id])
			flash.messageType = 'error'
			redirect(action: "list")
			return
		}
		def audited = WeeklyLimitB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[weeklyLimitBInstance: weeklyLimitBInstance, audited: audited]
    }

    def update(java.lang.String id, Long version) {
		def weeklyLimitBInstance
		try{
			weeklyLimitBInstance = weeklyLimitBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), id])
			flash.messageType = 'error'
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			weeklyLimitBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB')] as Object[],
						"Another user has updated this WeeklyLimitB while you were editing")
			  render(view: "edit", model: [weeklyLimitBInstance: weeklyLimitBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [weeklyLimitBInstance: weeklyLimitBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), weeklyLimitBInstance.id])
		flash.messageType = 'info'
		redirect(action: "show", id: weeklyLimitBInstance.id)
    }

    def delete(java.lang.String id) {
		try{
			def weeklyLimitBInstance = weeklyLimitBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), id])
			flash.messageType = 'info'
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), id])
			flash.messageType = 'error'
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), id])
			flash.messageType = 'error'
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def weeklyLimitBInstance = new WeeklyLimitB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB'), weeklyLimitBInstance.id])
		flash.messageType = 'error'
		render(view: "create", model: [weeklyLimitBInstance: weeklyLimitBInstance])
		return
	}
}
