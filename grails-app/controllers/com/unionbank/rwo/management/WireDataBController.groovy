package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY'])
class WireDataBController {

	def wireDataBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def wireDataBRevisionInstanceTotal = WireDataB.findAllRevisions().size()
		def audited = WireDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [wireDataBRevisionInstanceList: WireDataB.findAllRevisions(params).reverse(), wireDataBRevisionInstanceTotal: wireDataBRevisionInstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = WireDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [wireDataBInstanceList: WireDataB.list(params), wireDataBInstanceTotal: WireDataB.count(), audited: audited]
    }
	
	def show(java.lang.Long id) {
		def wireDataBInstance = WireDataB.get(id)
		if (!wireDataBInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), id])
			redirect(action: "list")
			return
		}
		def audited = WireDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[wireDataBInstance: wireDataBInstance, audited: audited]
	}

/*
    def create() {
		def audited = WireDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [wireDataBInstance: wireDataBService.create(params), audited: audited]
    }

    def save() {
		def wireDataBInstance
		try{
			wireDataBInstance = wireDataBService.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [wireDataBInstance: wireDataBInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), wireDataBInstance.id])
		redirect(action: "show", id: wireDataBInstance.id)
    }

    def edit(java.lang.Long id) {
		def wireDataBInstance
		try{
			wireDataBInstance = wireDataBService.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), id])
			redirect(action: "list")
			return
		}
		def audited = WireDataB.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[wireDataBInstance: wireDataBInstance, audited: audited]
    }

    def update(java.lang.Long id, Long version) {
		def wireDataBInstance
		try{
			wireDataBInstance = wireDataBService.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			wireDataBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'wireDataB.label', default: 'WireDataB')] as Object[],
						"Another user has updated this WireDataB while you were editing")
			  render(view: "edit", model: [wireDataBInstance: wireDataBInstance])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [wireDataBInstance: wireDataBInstance])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), wireDataBInstance.id])
		redirect(action: "show", id: wireDataBInstance.id)
    }

    def delete(java.lang.Long id) {
		try{
			def wireDataBInstance = wireDataBService.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def wireDataBInstance = new WireDataB(params)
		log.error "DuplicateKeyException ${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: 'wireDataB.label', default: 'WireDataB'), wireDataBInstance.id])
		render(view: "create", model: [wireDataBInstance: wireDataBInstance])
		return
	}
*/
}
