import pl.refaktor.enversdemo.*
import com.unionbank.rwo.management.*


class BootStrap {

    def init = { servletContext ->
		println "\nBootStrap : RUNTIME CLASSPATH\n***  "
		for (String property: System.getProperty("java.class.path").split(";")) {
			println property
		}
		println "***\n"
		
        environments {
            development {
                if (false && SystemDataB.count() == 0) {
					createSystemDataB1()
					createSystemDataB2()
					createSystemDataB3()
					createSystemDataB4()
					createSystemDataB5()
					createSystemDataB6()
					createSystemDataB7()
					createSystemDataB8()
					createSystemDataB9()
					createSystemDataB10()
					createSystemDataB11()
					createSystemDataB12()
					createSystemDataB13()
					createSystemDataB14()
					createSystemDataB15()
					createSystemDataB16()
					createSystemDataB17()
					createSystemDataB18()
					createSystemDataB19()
					createSystemDataB20()
					createSystemDataB21()
					
					BankInfoB bankInfoB1 = createBankInfoB1()
					BankInfoB bankInfoB2 = createBankInfoB2()
					BankInfoB bankInfoB3 = createBankInfoB3()
					createBankInfoB4()
					createBankInfoB5()
					
					
//					createWireDataB(1)
					createStatusB(1)
					createStatusB(2)
					createStatusB(3)
					createBeneficiaryB(1)
					createWireDataB(1)
					
                }
            }
        }
    }
    def destroy = {
    }

	
	void createSystemDataB1() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 1', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
		
	void createSystemDataB2() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 2', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}

		
	void createSystemDataB3() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 3', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	
	void createSystemDataB4() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 4', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	

	void createSystemDataB5() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 5', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	
	void createSystemDataB6() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 6', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB7() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 7', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB8() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 8', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB9() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 9', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB10() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 10', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB11() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 11', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB12() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 12', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB13() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 13', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB14() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 14', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB15() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 15', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB16() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 16', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB17() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 17', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB18() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 18', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB19() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 19', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB20() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 20', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	void createSystemDataB21() {
		SystemDataB.withTransaction {
			def systemDataB = new SystemDataB(keyName: 'key 21', keyValue: 'Key Val 1')
			systemDataB.save(failOnError: true, flush: true)
		}
	}
	
	
	void createBankInfoB1() {
		BankInfoB.withTransaction {
			def bankInfoB1 = new BankInfoB(abaNbr: 'abaNbr 1', bankName: 'bankName 1')
			bankInfoB1.save(failOnError: true, flush: true)
		}
	}
	
	
	void createBankInfoB2() {
		BankInfoB.withTransaction {
			def bankInfoB2 = new BankInfoB(abaNbr: 'abaNbr 2', bankName: 'bankName 1')
			bankInfoB2.save(failOnError: true, flush: true)
		}
	}
	void createBankInfoB3() {
		BankInfoB.withTransaction {
			def bankInfoB = new BankInfoB(abaNbr: 'abaNbr 3', bankName: 'bankName 3')
			bankInfoB.save(failOnError: true, flush: true)
		}
	}
	void createBankInfoB4() {
		BankInfoB.withTransaction {
			def bankInfoB = new BankInfoB(abaNbr: 'abaNbr 4', bankName: 'bankName 4')
			bankInfoB.save(failOnError: true, flush: true)
		}
	}
	void createBankInfoB5() {
		BankInfoB.withTransaction {
			def bankInfoB = new BankInfoB(abaNbr: 'abaNbr 5', bankName: 'bankName 5')
			bankInfoB.save(failOnError: true, flush: true)
		}
	}
	
	void createBeneficiaryB(int id) {
		BeneficiaryB.withTransaction {
		BankInfoB bankInfoB = new BankInfoB(abaNbr: 'abaNbr b' + id, bankName: 'bankName b' + id)
		bankInfoB.save(failOnError: true, flush: true)
			def beneficiaryB = new BeneficiaryB(
				cifNbr: 'cif-' + id,
				ssoId: 'sso-' + id,
				bnfciryName: 'bnfciryName ' + id,
				acctNbr: 'acctNbr ' + id,
				addrLineOne: 'addrLineOne ' + id,
				addrLineTwo: 'addrLineTwo ' + id,
				addrLineThree: 'addrLineThree ' + id,
				city: 'city ' + id,
				state: 'state ' + id,
				postalCd: 'postalCd ' + id,
				country: 'country ' + id,
				actvFlg: 'A',
				createDt: new Date(),
				updateDt: new Date())
			beneficiaryB.bank_Info = bankInfoB
			beneficiaryB.save(failOnError: true, flush: true)
		}
	}
	
	
	
	void createStatusB(int id) {
		StatusB.withTransaction {
			def statusB = new StatusB(statusCd: 'A' + id, description: 'description ' + id)
			statusB.save(failOnError: true, flush: true)
		}
	}
	
	void createWireDataB(int id) {
		WireDataB.withTransaction {
			def bankInfoB = new BankInfoB(abaNbr: 'abaNbr -w1', bankName: 'bankName w1')
			bankInfoB.save(failOnError: true, flush: true)
			def wireDataB = new WireDataB(sequenceNbr: id,
				 cifNbr: 'cif-' + id, ssoId: 'sso-' + id, channelType: 'ct-' + id,
				 trnsfrAmt: 200 + id, transactionFee: 20 + id, trnsfrDt: new Date(), trnsfrUpdtDt: new Date())
			wireDataB.bank_Info = bankInfoB
			wireDataB.bnfciry = BeneficiaryB.get(1 as Long)
			wireDataB.status = StatusB.get('A1' as String)
			
			wireDataB.save(failOnError: true, flush: true)
		}
	}
	
}
