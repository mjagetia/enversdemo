dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development2 {
        dataSource {
            dbCreate = "create" // one of 'create', 'create-drop', 'update', 'validate', ''
		    url = "jdbc:mysql://localhost:3306/orwo2"
		    driverClassName = "com.mysql.jdbc.Driver"
		    dialect = org.hibernate.dialect.MySQL5InnoDBDialect
		    username = "orwoapp"
		    password = "m938akww"
		}
    }
    development {
		dataSource {
			dbCreate = "validate"
//			dialect = "org.hibernate.dialect.Oracle10gDialect"
			url = "jdbc:oracle:thin:@chwdbdev207.unionbank.com:1525:E514D2"
			driverClassName = "oracle.jdbc.OracleDriver"
			username = "orwo"
			password = "ma55quyp"
			properties {
				validationQuery="select 1 from dual"
				testWhileIdle=true
				timeBetweenEvictionRunsMillis=60000
			}
		}
//        dataSource {
//            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
//            url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
//        }
    }
    test {
        dataSource {
			jndiName = "java:/RWOA_DS"
		}
    }
    testm {
        dataSource {
			dbCreate = "validate"
			dialect = "org.hibernate.dialect.Oracle10gDialect"
			url = "jdbc:oracle:thin:@chwdbdev214.unionbank.com:1525:e514"
			driverClassName = "oracle.jdbc.OracleDriver"  
			username = "orwoapp"
			password = "bqughe2m"	
			properties {
				validationQuery="select 1 from dual"
				testWhileIdle=true
				timeBetweenEvictionRunsMillis=60000
			}			
		}
    }
    test2 {
        dataSource {
			dbCreate = "validate"
			dialect = "org.hibernate.dialect.Oracle10gDialect"
			url = "jdbc:oracle:thin:@chwdbdev214.unionbank.com:1525:e514d2"
			driverClassName = "oracle.jdbc.OracleDriver"  
			username = "orwoapp"
			password = "bqughe2m"	
			properties {
				validationQuery="select 1 from dual"
				testWhileIdle=true
				timeBetweenEvictionRunsMillis=60000
			}			
		}
    }	
    production {
		dataSource {
			jndiName = "java:/RWOA_DS"
		}
    }
}

environments {
	test {
	  loggingSql = true
	}
	development {
	  loggingSql = true
	}
 }