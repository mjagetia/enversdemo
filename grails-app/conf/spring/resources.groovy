import com.unionbank.rwo.management.SpringSecurityServiceHolder
import com.unionbank.auth.ldap.LdapUserDetailsContextMapper

// Place your Spring DSL code here
beans = {
	
	ldapUserDetailsMapper(com.unionbank.auth.ldap.LdapUserDetailsContextMapper) {
	}
	
    springSecurityServiceHolder(SpringSecurityServiceHolder, ref('springSecurityService'))

    // Change it if you want to setup some Envers' configuration options.
    // System.setProperty('org.hibernate.envers.audit_table_prefix', 'AUD_')
    // System.setProperty('org.hibernate.envers.audit_table_suffix', '')
	
	
}
