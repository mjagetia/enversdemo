package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class WireDataBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def wireDataBInstance = new WireDataB(params)
			if (!wireDataBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return wireDataBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new WireDataB(params)
	}

	def edit(String id) {
		def wireDataBInstance = WireDataB.get(id)
		if (!wireDataBInstance) {
			throw new GenericEditException();
		}
		return wireDataBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def wireDataBInstance = WireDataB.get(id)
		if (!wireDataBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (wireDataBInstance.version > version) {
				wireDataBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'wireDataB.label', default: 'WireDataB')] as Object[],
					"Another user has updated this WireDataB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		wireDataBInstance.properties = params
		if (!wireDataBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return wireDataBInstance
	}

	def delete(String id) {
		def wireDataBInstance = WireDataB.get(id)
		if (!wireDataBInstance) {
			throw new GenericNotFoundException()
		}

		wireDataBInstance.delete(flush: true)
	}
	
}