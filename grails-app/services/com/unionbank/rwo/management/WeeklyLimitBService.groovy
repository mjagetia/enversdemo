package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class WeeklyLimitBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def weeklyLimitBInstance = new WeeklyLimitB(params)
			if (!weeklyLimitBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return weeklyLimitBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new WeeklyLimitB(params)
	}

	def edit(String id) {
		def weeklyLimitBInstance = WeeklyLimitB.get(id)
		if (!weeklyLimitBInstance) {
			throw new GenericEditException();
		}
		return weeklyLimitBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def weeklyLimitBInstance = WeeklyLimitB.get(id)
		if (!weeklyLimitBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (weeklyLimitBInstance.version > version) {
				weeklyLimitBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB')] as Object[],
					"Another user has updated this WeeklyLimitB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		weeklyLimitBInstance.properties = params
		if (!weeklyLimitBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return weeklyLimitBInstance
	}

	def delete(String id) {
		def weeklyLimitBInstance = WeeklyLimitB.get(id)
		if (!weeklyLimitBInstance) {
			throw new GenericNotFoundException()
		}

		weeklyLimitBInstance.delete(flush: true)
	}
	
}