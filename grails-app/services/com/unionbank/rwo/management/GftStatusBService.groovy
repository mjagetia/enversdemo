package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class GftStatusBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def gftStatusBInstance = new GftStatusB(params)
			if (!gftStatusBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return gftStatusBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new GftStatusB(params)
	}

	def edit(String id) {
		def gftStatusBInstance = GftStatusB.get(id)
		if (!gftStatusBInstance) {
			throw new GenericEditException();
		}
		return gftStatusBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def gftStatusBInstance = GftStatusB.get(id)
		if (!gftStatusBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (gftStatusBInstance.version > version) {
				gftStatusBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'gftStatusB.label', default: 'GftStatusB')] as Object[],
					"Another user has updated this GftStatusB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		gftStatusBInstance.properties = params
		if (!gftStatusBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return gftStatusBInstance
	}

	def delete(String id) {
		def gftStatusBInstance = GftStatusB.get(id)
		if (!gftStatusBInstance) {
			throw new GenericNotFoundException()
		}

		gftStatusBInstance.delete(flush: true)
	}
	
}