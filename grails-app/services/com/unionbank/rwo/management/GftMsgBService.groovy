package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class GftMsgBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def gftMsgBInstance = new GftMsgB(params)
			if (!gftMsgBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return gftMsgBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new GftMsgB(params)
	}

	def edit(String id) {
		def gftMsgBInstance = GftMsgB.get(id)
		if (!gftMsgBInstance) {
			throw new GenericEditException();
		}
		return gftMsgBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def gftMsgBInstance = GftMsgB.get(id)
		if (!gftMsgBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (gftMsgBInstance.version > version) {
				gftMsgBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'gftMsgB.label', default: 'GftMsgB')] as Object[],
					"Another user has updated this GftMsgB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		gftMsgBInstance.properties = params
		if (!gftMsgBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return gftMsgBInstance
	}

	def delete(String id) {
		def gftMsgBInstance = GftMsgB.get(id)
		if (!gftMsgBInstance) {
			throw new GenericNotFoundException()
		}

		gftMsgBInstance.delete(flush: true)
	}
	
}