package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class PricingBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def pricingBInstance = new PricingB(params)
			if (!pricingBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return pricingBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new PricingB(params)
	}

	def edit(String id) {
		def pricingBInstance = PricingB.get(id)
		if (!pricingBInstance) {
			throw new GenericEditException();
		}
		return pricingBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def pricingBInstance = PricingB.get(id)
		if (!pricingBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (pricingBInstance.version > version) {
				pricingBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'pricingB.label', default: 'PricingB')] as Object[],
					"Another user has updated this PricingB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		pricingBInstance.properties = params
		if (!pricingBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return pricingBInstance
	}

	def delete(String id) {
		def pricingBInstance = PricingB.get(id)
		if (!pricingBInstance) {
			throw new GenericNotFoundException()
		}

		pricingBInstance.delete(flush: true)
	}
	
}