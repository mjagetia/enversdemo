package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class StatusBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def statusBInstance = new StatusB(params)
			if (!statusBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return statusBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new StatusB(params)
	}

	def edit(String id) {
		def statusBInstance = StatusB.get(id)
		if (!statusBInstance) {
			throw new GenericEditException();
		}
		return statusBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def statusBInstance = StatusB.get(id)
		if (!statusBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (statusBInstance.version > version) {
				statusBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'statusB.label', default: 'StatusB')] as Object[],
					"Another user has updated this StatusB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		statusBInstance.properties = params
		if (!statusBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return statusBInstance
	}

	def delete(String id) {
		def statusBInstance = StatusB.get(id)
		if (!statusBInstance) {
			throw new GenericNotFoundException()
		}

		statusBInstance.delete(flush: true)
	}
	
}