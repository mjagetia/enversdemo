package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class BeneficiaryBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def beneficiaryBInstance = new BeneficiaryB(params)
			if (!beneficiaryBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return beneficiaryBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new BeneficiaryB(params)
	}

	def edit(String id) {
		def beneficiaryBInstance = BeneficiaryB.get(id)
		if (!beneficiaryBInstance) {
			throw new GenericEditException();
		}
		return beneficiaryBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def beneficiaryBInstance = BeneficiaryB.get(id)
		if (!beneficiaryBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (beneficiaryBInstance.version > version) {
				beneficiaryBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'beneficiaryB.label', default: 'BeneficiaryB')] as Object[],
					"Another user has updated this BeneficiaryB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		beneficiaryBInstance.properties = params
		if (!beneficiaryBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return beneficiaryBInstance
	}

	def delete(String id) {
		def beneficiaryBInstance = BeneficiaryB.get(id)
		if (!beneficiaryBInstance) {
			throw new GenericNotFoundException()
		}

		beneficiaryBInstance.delete(flush: true)
	}
	
}