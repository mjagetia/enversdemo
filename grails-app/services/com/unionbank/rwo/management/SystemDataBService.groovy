package com.unionbank.rwo.management

import org.springframework.dao.DataIntegrityViolationException

class SystemDataBService {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def systemDataBInstance = new SystemDataB(params)
			if (!systemDataBInstance.save(flush: true)) {
				throw new GenericSaveException();
			}
		return systemDataBInstance
	}
	
	def create(Map<Object, Object> params) {
		return new SystemDataB(params)
	}

	def edit(String id) {
		def systemDataBInstance = SystemDataB.get(id)
		if (!systemDataBInstance) {
			throw new GenericEditException();
		}
		return systemDataBInstance
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def systemDataBInstance = SystemDataB.get(id)
		if (!systemDataBInstance) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (systemDataBInstance.version > version) {
				systemDataBInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: 'systemDataB.label', default: 'SystemDataB')] as Object[],
					"Another user has updated this SystemDataB while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		systemDataBInstance.properties = params
		if (!systemDataBInstance.save(flush: true)) {
			throw new GenericSaveException();
		}
		return systemDataBInstance
	}

	def delete(String id) {
		def systemDataBInstance = SystemDataB.get(id)
		if (!systemDataBInstance) {
			throw new GenericNotFoundException()
		}

		systemDataBInstance.delete(flush: true)
	}
	
}