package com.unionbank.rwo.management

class StatusB {

	String statusCd
	String description

	static hasMany = [gftMsgBs: GftMsgB,
	                  wireDataBs: WireDataB]

	static mapping = {
		id name: "statusCd", generator: "assigned"
		version false
		table "STATUS_B"
	}

	static constraints = {
		statusCd maxSize: 3
		description nullable: true, maxSize: 16
	}
}
