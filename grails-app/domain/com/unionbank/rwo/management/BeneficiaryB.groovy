package com.unionbank.rwo.management

import groovy.transform.ToString

@ToString (ignoreNulls = true, includeNames=false, includeFields=true, 
	includePackage = false, includeSuper=false,
	excludes="ssoId,bnfciryName,addrLineOne,addrLineTwo,addrLineThree,city,state,postalCd,country,createDt,updateDt")
class BeneficiaryB {

	Long bnfciryId
	String cifNbr
	String ssoId
	String bnfciryName
	String acctNbr
	String addrLineOne
	String addrLineTwo
	String addrLineThree
	String city
	String state
	String postalCd
	String country
	Character actvFlg
	Date createDt
	Date updateDt
	BankInfoB bank_Info

	static hasMany = [wireDataBs: WireDataB]
	static belongsTo = [BankInfoB]

	static mapping = {
		id column: "BNFCIRY_ID", name: "bnfciryId"
		version false
		table "BENEFICIARY_B"
	}

	static constraints = {
		cifNbr maxSize: 12
		ssoId maxSize: 32
		bnfciryName nullable: true
		acctNbr nullable: true, maxSize: 24
		addrLineOne nullable: true
		addrLineTwo nullable: true
		addrLineThree nullable: true
		city nullable: true
		state nullable: true, maxSize: 128
		postalCd nullable: true, maxSize: 24
		country nullable: true, maxSize: 128
		actvFlg maxSize: 1
	}
	
	def getId(){
		bnfciryId
	}
}
