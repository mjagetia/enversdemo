package com.unionbank.rwo.management

import java.sql.Clob
import java.sql.Timestamp;

class GftMsgB {

	Long msgId
	String direction
	Timestamp dtStored
	Clob message
	Date createDt
	Date updateDt
	WireDataB wire_data
	StatusB rwo_status

	static belongsTo = [StatusB, WireDataB]

	static mapping = {
		id column: "MSG_ID", name: "msgId"
		version false
		table "GFT_MSG_B"
		rwo_status column: "rwo_status_cd"
		wire_data column: "transaction_id"
	}

	static constraints = {
		direction nullable: true, maxSize: 8
		dtStored nullable: true
		message nullable: true, type:'materialized_clob'
	}
}
