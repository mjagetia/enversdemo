package com.unionbank.rwo.management
import org.hibernate.envers.Audited

@Audited
class GftStatusB {

	String errorCd
	String errorMsg

	static mapping = {
		id name: "errorCd", generator: "assigned"
		version false
		table "GFT_STATUS_B"
	}

	static constraints = {
		errorCd maxSize: 4
		errorMsg nullable: true
	}
	
	String getId(){
		return errorCd
	}
	
	def setId(String id){
		errorCd = id
	}
}
