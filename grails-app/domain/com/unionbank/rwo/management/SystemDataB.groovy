package com.unionbank.rwo.management

import groovy.transform.ToString
import org.hibernate.envers.Audited

@ToString (ignoreNulls = true, includeNames=false, includeFields=true, 
	includePackage = false, includeSuper=false,
	excludes="description")
@Audited
class SystemDataB {

	String id
	String keyName
	String keyValue
	String description

	static mapping = {
		id column: "key_name", name: "keyName", generator: "assigned", type: "string"
		version false
		table "SYSTEM_DATA_B"		
	}
	
	static constraints = {
		keyName blank:false, nullable:false, unique: true
		keyValue blank:false, nullable:false
		description nullable: true, maxSize: 1024, widget: 'textarea'
	}

	
//	static transients = ['keyName']
	
	void setKeyName(String keyName) {
		id = keyName
	 }
	 String getKeyName() {
		return id
	 }
	 
}
