package com.unionbank.rwo.management

import groovy.transform.ToString

@ToString (ignoreNulls = true, includeNames=false, includeFields=true, 
	includePackage = false, includeSuper=false,
	excludes="addrLineTwo,addrLineThree,city,state,country")
class BankInfoB {

	Long bankInfoId
	String abaNbr
	String bankName
	String addrLineOne
	String addrLineTwo
	String addrLineThree
	String city
	String state
	String postalCd
	String country
	String entityRole

	static hasMany = [beneficiaryBs: BeneficiaryB,
		wireDataBs: WireDataB]

	static mapping = {
		id column: "BANK_INFO_ID", name: "bankInfoId"
		version false
		table "BANK_INFO_B"
	}

	static constraints = {
		abaNbr maxSize: 24
		bankName nullable: true
		addrLineOne nullable: true
		addrLineTwo nullable: true
		addrLineThree nullable: true
		city nullable: true
		state nullable: true, maxSize: 128
		postalCd nullable: true, maxSize: 24
		country nullable: true, maxSize: 128
		entityRole nullable: true, maxSize: 3
	}

	def getId(){
		bankInfoId
	}
}
