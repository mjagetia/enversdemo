package com.unionbank.rwo.management

import org.hibernate.envers.Audited

@Audited
class PricingB {

	String id
	String code
	String description
	BigDecimal fee

	static mapping = {
		id column: "code", name: "code", generator: "assigned", type: "string"
		version false
		table "PRICING_B"
	}

	static constraints = {
		code maxSize: 8, blank:false, nullable:false, unique: true
		description nullable: true, maxSize: 128
		fee nullable: true
	}
	
//	static transients = ['code']
	
	void setCode(String code) {
		id = code
	 }
	 String getCode() {
		return id
	 }
	
}
