package com.unionbank.rwo.management

class WireDataB {

	Long transactionId
	Long sequenceNbr
	String cifNbr
	String ssoId
	String sendAccountNumber
	String sendAccountCode
	String sendAccountDesc
	String channelType
	BigDecimal trnsfrAmt
	BigDecimal transactionFee
	Date trnsfrDt
	Date deliveryDt
	Date trnsfrUpdtDt
	Date gftResponseDt
	String obiComment
	String purposeOfWire
	BigDecimal gftTransId
	BankInfoB bank_Info
	StatusB status
	BeneficiaryB bnfciry

	static hasMany = [gftMsgBs: GftMsgB]
	static belongsTo = [BankInfoB, BeneficiaryB, StatusB]

	static mapping = {
		id column: "TRANSACTION_ID", name: "transactionId"
		version false
		table "WIRE_DATA_B"
		status column: "status_cd"
	}

	static constraints = {
		sequenceNbr nullable: true
		cifNbr maxSize: 12
		ssoId maxSize: 32
		sendAccountNumber nullable: true, maxSize: 32
		sendAccountCode nullable: true, maxSize: 12
		sendAccountDesc nullable: true, maxSize: 140
		channelType maxSize: 5
		trnsfrAmt scale: 4
		transactionFee scale: 4
		deliveryDt nullable: true
		gftResponseDt nullable: true
		obiComment nullable: true, maxSize: 140
		purposeOfWire nullable: true, maxSize: 140
		gftTransId nullable: true
	}
	
	Long getId(){
		transactionId
	}
}
