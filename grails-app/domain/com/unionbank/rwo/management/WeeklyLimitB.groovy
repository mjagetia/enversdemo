package com.unionbank.rwo.management

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.envers.Audited

@Audited
class WeeklyLimitB implements Serializable {

	String custSegment
	String appId
	BigDecimal code
	BigDecimal limit
	String description
	
	int hashCode() {
		def builder = new HashCodeBuilder()
		builder.append custSegment
		builder.append appId
		builder.toHashCode()
	}

	boolean equals(other) {
		if (other == null) return false
		def builder = new EqualsBuilder()
		builder.append custSegment, other.custSegment
		builder.append appId, other.appId
		builder.isEquals()
	}

	static mapping = {
		version false
		table "WEEKLY_LIMIT_B"
	}

	static constraints = {
		id type: 'integer'
		custSegment  maxSize: 40, nullable: false, unique: ['appId']
		appId 		maxSize: 4,  nullable: false
		limit  nullable: true
		code()
		description nullable: true, maxSize: 40, widget: 'textarea'
	}
	
}

