<%@ page import="com.unionbank.rwo.management.SystemDataB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'systemDataB.label', default: 'SystemDataB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
					<div class="sub-actions btn-group pull-right">
						<g:link class="list btn btn-info" action="list">
							<i class="icon-list icon-white"></i>
							<g:message code="default.list.label" args="[entityName]" />
						</g:link>
						<g:link class="revisions btn" action="revisions">
							<i class="icon-time"></i>
							<g:message code="default.revisions.label" args="[entityName]"/>
						</g:link>
						<g:link class="create btn btn-success" action="create">
							<i class="icon-plus icon-white"></i>
							<g:message code="default.create.label" args="[entityName]"/>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
			        <thead>
			        <tr>
			
			            <g:sortableColumn property="revisionEntity.id" title="${message(code: 'envers.id.label', default: 'Rev id')}"/>
			
			            <g:sortableColumn property="revisionType" title="${message(code: 'envers.revisionType.label', default: 'Rev type')}"/>
			
			            <g:sortableColumn property="revisionEntity.revisionDate" title="${message(code: 'envers.revisionDate.label', default: 'Rev date')}"/>
			
			            <g:sortableColumn property="revisionEntity.currentUser" title="${message(code: 'envers.currentUser.label', default: 'Rev author')}"/>
			
			            <g:sortableColumn property="revisionEntity.keyName" title="${message(code: 'envers.currentUser.label', default: 'Key Name')}"/>
			
			            <g:sortableColumn property="revisionEntity.keyValue" title="${message(code: 'envers.currentUser.label', default: 'Key Value')}"/>
			
			            <g:sortableColumn property="revisionEntity.description" title="${message(code: 'envers.currentUser.label', default: 'Description')}"/>
			
			        </tr>
			        </thead>
			        <tbody>
			        <g:each in="${systemDataBInstanceList}" status="i" var="systemDataBRevision">
			            <tr>
			                <td><g:fieldValue field="revisionEntity.id" bean="${systemDataBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionType" bean="${systemDataBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionEntity.revisionDate" bean="${systemDataBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionEntity.currentUser" bean="${systemDataBRevision}" /></td>

			                <td><g:link action="show" id="${systemDataBRevision.id}"><g:fieldValue field="keyName" bean="${systemDataBRevision}" /></g:link></td>

			                <td><g:fieldValue field="keyValue" bean="${systemDataBRevision}" /></td>

			                <td><g:fieldValue field="description" bean="${systemDataBRevision}" /></td>
			
			            </tr>
			        </g:each>
			        </tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${systemDataBInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
