<%@ page import="com.unionbank.rwo.management.SystemDataB" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'systemDataB.label', default: 'Booking')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-systemDataB" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                            default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-systemDataB" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="revisionEntity.id" title="${message(code: 'envers.id.label', default: 'Rev id')}"/>

            <g:sortableColumn property="revisionType" title="${message(code: 'envers.revisionType.label', default: 'Rev type')}"/>

            <g:sortableColumn property="revisionEntity.revisionDate" title="${message(code: 'envers.revisionDate.label', default: 'Rev date')}"/>

            <g:sortableColumn property="revisionEntity.currentUser" title="${message(code: 'envers.currentUser.label', default: 'Rev author')}"/>

            <g:sortableColumn property="revisionEntity.keyName" title="${message(code: 'envers.currentUser.label', default: 'Key Name')}"/>

            <g:sortableColumn property="revisionEntity.keyValue" title="${message(code: 'envers.currentUser.label', default: 'Key Value')}"/>

            <g:sortableColumn property="revisionEntity.description" title="${message(code: 'envers.currentUser.label', default: 'Description')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${systemDataBInstanceList}" status="i" var="systemDataBRevision">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:fieldValue field="revisionEntity.id" bean="${systemDataBRevision}" /></td>

                <td><g:link action="show" id="${systemDataBRevision?.id}"><g:fieldValue field="keyName" bean="${systemDataBRevision}" /></g:link></td>
                <td><g:link action="show" id="${systemDataBRevision?.id}"><g:fieldValue field="keyValue" bean="${systemDataBRevision}" /></g:link></td>

                <td><g:fieldValue field="revisionType" bean="${systemDataBRevision}" /></td>

                <td><g:fieldValue field="revisionEntity.revisionDate" bean="${systemDataBRevision}" /></td>

                <td><g:fieldValue field="revisionEntity.currentUser" bean="${systemDataBRevision}" /></td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${systemDataBInstanceTotal}"/>
    </div>
</div>
</body>
</html>
