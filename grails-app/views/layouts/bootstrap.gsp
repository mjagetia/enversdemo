<%@ page import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes" %>
<%@ page import="org.codehaus.groovy.grails.commons.GrailsClassUtils"%>
<%@ page import="com.unionbank.rwo.utils.UBGrailsClassUtils"%>
<% startTestDrive = params.get("testDriveRWOA") %>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<r:require modules="scaffolding"/>

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		
		<!-- Standard iPhone --> 
		<link rel="apple-touch-icon" sizes="57x57" href="${resource(dir: 'images', file: 'apple-touch-icon-iphone.png')}" />
		<!-- Retina iPhone --> 
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-iphone4.png')}" />
		<!-- Standard iPad --> 
		<link rel="apple-touch-icon" sizes="72x72" href="${resource(dir: 'images', file: 'apple-touch-icon-ipad.png')}" />
		<!-- Retina iPad --> 
		<link rel="apple-touch-icon" sizes="144x144" href="${resource(dir: 'images', file: 'apple-touch-icon-retina-ipad.png')}" />

		<link rel="stylesheet" href="${resource(dir: 'css', file: 'introjs.css')}" type="text/css" />
		<g:if test="${startTestDrive}">
			<r:script disposition='head'>
		    var startTestDrive = true;
		    </r:script>
		</g:if>
		
		<r:require module="jquery-ui"/>
		<g:layoutHead/>
		<r:layoutResources/>
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'rwo.css')}" type="text/css" />
	</head>

	<body>
		<nav id="top-nav" class="navbar navbar-fixed-top<sec:ifLoggedIn> navbar-inverse</sec:ifLoggedIn>">
			<div class="navbar-inner">
				<div class="container-fluid">
					
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					
					<a class="brand" href="${createLink(uri: '/')}"><span class="Parent-app-id">RWO</span>-<span class="app-module-id">A</span></a>

				<div class="nav-collapse">
						<sec:ifLoggedIn>
						<ul class="nav intro-nav-menu-top pull-right">							
							%{-- <li<%= request.forwardURI == "${createLink(uri: '/')}" ? ' class="active"' : '' %>><a href="${createLink(uri: '/')}">Home</a></li> --}%
							<g:each var="c" in="${grailsApplication.controllerClasses.findAll{UBGrailsClassUtils.isClassBelowPackage(it.clazz, ['com.unionbank.rwo.management']) }.sort{ it.getNaturalName() } }">
								<li<%= c.logicalPropertyName == controllerName ? ' class="active"' : '' %>><g:link controller="${c.logicalPropertyName}">${GrailsClassUtils.getLogicalName(GrailsClassUtils.getNaturalName(c.name), 'B')}</g:link></li>
							</g:each>
							<li>
								<ul class="nav intro-nav-auth-top pull-right">
									<li>
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">Howdy <span><sec:username /></span> <b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><g:link controller='logout'><span class="icon-off"></span> Logout</g:link></li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
						</sec:ifLoggedIn>
						<sec:ifNotLoggedIn>
						<ul class="nav intro-nav-menu-top pull-right">							
							<li>
							<g:link controller='login' class="active"><span class="icon-user"></span> Login</g:link>
							</li>
						</ul>
						</sec:ifNotLoggedIn>							
					</div>
				</div>				
			</div>
		</nav>

		<div class="container-fluid">
			<g:layoutBody/>

			<hr>

			<footer>
				<p>&copy; UnionBank 2014</p>
			</footer>
		</div>
	
		
		<!-- Modal -->
		<div id="keyboard-shortcuts" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Keyboard Shortcuts" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 id="myModalLabel">Kyboard Shortcuts</h3>
		  </div>
		  <div class="modal-body">
		    <ul>
				<li>use <em>h</em> key to go to Home</li>
				<li>use <em>c</em> key to go to Create an Item</li>
				<li>use <em>l</em> key to go to List Items/li>
				<li>use <em>r</em> key to go to See the Revision History</li>
				<li>use <em>?</em> key to go see this menu</li>
			</ul>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
		</div>

		<g:javascript src="jquery.placeholder.js" />
		<g:javascript src="jquery.sessionTimeout.js" />
		<g:javascript src="intro.js" />		
		<g:javascript src="mousetrap.js" />		
		<g:javascript src="application.js" />
		<r:layoutResources/>
	</body>
</html>