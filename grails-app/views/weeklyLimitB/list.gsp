
<%@ page import="com.unionbank.rwo.management.WeeklyLimitB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'weeklyLimitB.label', default: 'WeeklyLimitB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.list.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped table-800px center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">
						
							<g:sortableColumn property="custSegment" title="${message(code: 'weeklyLimitB.custSegment.label', default: 'Cust Segment')}" />
						
							<g:sortableColumn property="appId" title="${message(code: 'weeklyLimitB.appId.label', default: 'App Id')}" />
						
							<g:sortableColumn property="limit" title="${message(code: 'weeklyLimitB.limit.label', default: 'Wire Limit')}" />
						
							<g:sortableColumn property="description" title="${message(code: 'weeklyLimitB.description.label', default: 'Description')}" />
						
							<g:sortableColumn property="code" title="${message(code: 'weeklyLimitB.code.label', default: 'Code')}" />
						
							<th class="pull-right">Operations</th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${weeklyLimitBInstanceList}" var="weeklyLimitBInstance">
						<tr>
						
							<td>${fieldValue(bean: weeklyLimitBInstance, field: "custSegment")}</td>
						
							<td>${fieldValue(bean: weeklyLimitBInstance, field: "appId")}</td>
						
							<td>${fieldValue(bean: weeklyLimitBInstance, field: "limit")}</td>
						
							<td>${fieldValue(bean: weeklyLimitBInstance, field: "description")}</td>
						
							<td>${fieldValue(bean: weeklyLimitBInstance, field: "code")}</td>
						
							<td class="link text-right intro-entity-button-functions">
									<g:link action="show"
										id="${weeklyLimitBInstance.id}" class="btn btn-success btn-mini intro-entity-button-show">
										<i class="icon-search icon-white"></i><span class="hidden-phone"> Show</span>
									</g:link>
									 <g:link
										class="btn btn-primary btn-mini intro-entity-button-edit }" action="edit"
										id="${weeklyLimitBInstance?.id}">
										<i class="icon-pencil icon-white"></i><span class="hidden-phone"> <g:message code="default.button.edit.label" default="Edit" /></span>
									</g:link> 
							</td>
						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-centered">
					<bootstrap:paginate total="${weeklyLimitBInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>