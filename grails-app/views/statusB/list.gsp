
<%@ page import="com.unionbank.rwo.management.StatusB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'statusB.label', default: 'StatusB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.list.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped table-800px center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">
						
							<g:sortableColumn property="statusCd" title="${message(code: 'statusB.statusCd.label', default: 'Status Cd')}" />
						
							<g:sortableColumn property="description" title="${message(code: 'statusB.description.label', default: 'Description')}" />
						
							<th class="pull-right">Operations</th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${statusBInstanceList}" var="statusBInstance">
						<tr>
						
							<td>${fieldValue(bean: statusBInstance, field: "statusCd")}</td>
						
							<td>${fieldValue(bean: statusBInstance, field: "description")}</td>
						
							<td class="link text-right intro-entity-button-functions">
									<g:link action="show"
										id="${statusBInstance.id}" class="btn btn-success btn-mini intro-entity-button-show">
										<i class="icon-search icon-white"></i><span class="hidden-phone"> Show</span>
									</g:link>
									 <g:link
										class="btn btn-primary btn-mini intro-entity-button-edit }" action="edit"
										id="${statusBInstance?.id}">
										<i class="icon-pencil icon-white"></i><span class="hidden-phone"> <g:message code="default.button.edit.label" default="Edit" /></span>
									</g:link> 
									<g:form class="form-inline display-inline" action="edit" id="${statusBInstance?.id}" >
										<g:hiddenField name="version" value="${statusBInstance?.version}" />
										<g:hiddenField name="id" value="${statusBInstance?.id}" />
										<button type="submit" class="btn btn-danger btn-mini intro-entity-button-delete" name="_action_delete" formnovalidate onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
											<i class="icon-trash icon-white"></i><span class="hidden-phone"> <g:message code="default.button.delete.label" default="Delete" /></span>
										</button>							
									</g:form>
							</td>
						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-centered">
					<bootstrap:paginate total="${statusBInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>