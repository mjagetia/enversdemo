
<%@ page import="com.unionbank.rwo.management.BeneficiaryB" %>

<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'beneficiaryB.label', default: 'BeneficiaryB')}" />
		<title><g:message code="default.revisions.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.revisions.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn" id="entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">

			            <g:sortableColumn property="revisionEntity.id" title="${message(code: 'envers.id.label', default: 'Rev id')}"/>
			
			            <g:sortableColumn property="revisionType" title="${message(code: 'envers.revisionType.label', default: 'Rev type')}"/>
			
			            <g:sortableColumn property="revisionEntity.revisionDate" title="${message(code: 'envers.revisionDate.label', default: 'Rev date')}"/>
			
			            <g:sortableColumn property="revisionEntity.currentUser" title="${message(code: 'envers.currentUser.label', default: 'Rev author')}"/>
						
						
						
						
							<g:sortableColumn property="cifNbr" title="${message(code: 'beneficiaryB.cifNbr.label', default: 'Cif Nbr')}" />
						
							<g:sortableColumn property="ssoId" title="${message(code: 'beneficiaryB.ssoId.label', default: 'Sso Id')}" />
						
							<g:sortableColumn property="bnfciryName" title="${message(code: 'beneficiaryB.bnfciryName.label', default: 'Bnfciry Name')}" />
						
							<g:sortableColumn property="acctNbr" title="${message(code: 'beneficiaryB.acctNbr.label', default: 'Acct Nbr')}" />
						
							<g:sortableColumn property="addrLineOne" title="${message(code: 'beneficiaryB.addrLineOne.label', default: 'Addr Line One')}" />
						
							<g:sortableColumn property="addrLineTwo" title="${message(code: 'beneficiaryB.addrLineTwo.label', default: 'Addr Line Two')}" />
						
						</tr>
					</thead>
					<tbody>
					<g:each in="${beneficiaryBRevisionInstanceList}" var="beneficiaryBRevision">
						<tr>
					<td><g:fieldValue field="revisionEntity.id" bean="${beneficiaryBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionType" bean="${beneficiaryBRevision}" /></td>
						
			                <td><g:fieldValue field="revisionEntity.revisionDate" bean="${beneficiaryBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionEntity.currentUser" bean="${beneficiaryBRevision}" /></td>

						
							<td>${fieldValue(bean: beneficiaryBRevision, field: "cifNbr")}</td>
						
							<td>${fieldValue(bean: beneficiaryBRevision, field: "ssoId")}</td>
						
							<td>${fieldValue(bean: beneficiaryBRevision, field: "bnfciryName")}</td>
						
							<td>${fieldValue(bean: beneficiaryBRevision, field: "acctNbr")}</td>
						
							<td>${fieldValue(bean: beneficiaryBRevision, field: "addrLineOne")}</td>
						
							<td>${fieldValue(bean: beneficiaryBRevision, field: "addrLineTwo")}</td>
						

						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-right">
					<bootstrap:paginate total="${beneficiaryBRevisionInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
