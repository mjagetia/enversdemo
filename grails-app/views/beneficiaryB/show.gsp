
<%@ page import="com.unionbank.rwo.management.BeneficiaryB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'beneficiaryB.label', default: 'BeneficiaryB')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">

				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.show.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl class="block-800px center">
				
					<g:if test="${beneficiaryBInstance?.cifNbr}">
						<dt><g:message code="beneficiaryB.cifNbr.label" default="Cif Nbr" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="cifNbr"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.ssoId}">
						<dt><g:message code="beneficiaryB.ssoId.label" default="Sso Id" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="ssoId"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.bnfciryName}">
						<dt><g:message code="beneficiaryB.bnfciryName.label" default="Bnfciry Name" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="bnfciryName"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.acctNbr}">
						<dt><g:message code="beneficiaryB.acctNbr.label" default="Acct Nbr" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="acctNbr"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.addrLineOne}">
						<dt><g:message code="beneficiaryB.addrLineOne.label" default="Addr Line One" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="addrLineOne"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.addrLineTwo}">
						<dt><g:message code="beneficiaryB.addrLineTwo.label" default="Addr Line Two" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="addrLineTwo"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.addrLineThree}">
						<dt><g:message code="beneficiaryB.addrLineThree.label" default="Addr Line Three" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="addrLineThree"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.city}">
						<dt><g:message code="beneficiaryB.city.label" default="City" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="city"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.state}">
						<dt><g:message code="beneficiaryB.state.label" default="State" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="state"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.postalCd}">
						<dt><g:message code="beneficiaryB.postalCd.label" default="Postal Cd" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="postalCd"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.country}">
						<dt><g:message code="beneficiaryB.country.label" default="Country" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="country"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.actvFlg}">
						<dt><g:message code="beneficiaryB.actvFlg.label" default="Actv Flg" /></dt>
						
							<dd><g:fieldValue bean="${beneficiaryBInstance}" field="actvFlg"/></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.bank_Info}">
						<dt><g:message code="beneficiaryB.bank_Info.label" default="Bank Info" /></dt>
						
							<dd><g:link controller="bankInfoB" action="show" id="${beneficiaryBInstance?.bank_Info?.id}">${beneficiaryBInstance?.bank_Info?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.updateDt}">
						<dt><g:message code="beneficiaryB.updateDt.label" default="Update Dt" /></dt>
						
							<dd><g:formatDate date="${beneficiaryBInstance?.updateDt}" /></dd>
						
					</g:if>
				
					<g:if test="${beneficiaryBInstance?.wireDataBs}">
						<dt><g:message code="beneficiaryB.wireDataBs.label" default="Wire Data Bs" /></dt>
						
							<g:each in="${beneficiaryBInstance.wireDataBs}" var="w">
							<dd><g:link controller="wireDataB" action="show" id="${w.id}">${w?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
				</dl>

			<g:form class="block-800px center">
				<g:hiddenField name="id" value="${beneficiaryBInstance?.id}" />
				<div class="form-actions">
					<div class="block-800px center">
						<g:link class="list btn" action="list">
							<i class="icon-arrow-left"></i>
							<g:message code="default.button.back.label" default="Back" />
						</g:link>
					</div>
				</div>
			</g:form>

		</div>

		</div>
	</body>
</html>
