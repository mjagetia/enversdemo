<%@ page import="com.unionbank.rwo.management.GftMsgB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'gftMsgB.label', default: 'GftMsgB')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.edit.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				
			
			<div>
				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">${flash.message}</bootstrap:alert>
				</g:if>

				<g:hasErrors bean="${gftMsgBInstance}">
				<bootstrap:alert class="alert-error block-800px center">
				<ul>
					<g:eachError bean="${gftMsgBInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>

				<fieldset>
					<g:form class="form-horizontal block-800px center" action="edit" id="${gftMsgBInstance?.id}" >
						<g:hiddenField name="version" value="${gftMsgBInstance?.version}" />
						<fieldset>
							<f:display bean="gftMsgBInstance" property="keyName"/>					
							<f:all bean="gftMsgBInstance" except="id,keyName"/>
							<div class="form-actions">
								<button type="submit" class="btn btn-primary" name="_action_update">
									<i class="icon-ok icon-white"></i>
									<g:message code="default.button.update.label" default="Update" />
								</button>
								<button type="submit" class="btn btn-danger" name="_action_delete" formnovalidate onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
									<i class="icon-trash icon-white"></i>
									<g:message code="default.button.delete.label" default="Delete" />
								</button>
								<g:link class="list btn" action="list">
									<i class="icon-arrow-left"></i>
									<g:message code="default.button.cancel.label" default="Cancel" />
								</g:link>
							</div>
						</fieldset>
					</g:form>
				</fieldset>
				</div>
			</div>
		</div>
	</body>
</html>
