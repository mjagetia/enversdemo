
<%@ page import="com.unionbank.rwo.management.GftMsgB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'gftMsgB.label', default: 'GftMsgB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.list.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped table-800px center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">
						
							<g:sortableColumn property="direction" title="${message(code: 'gftMsgB.direction.label', default: 'Direction')}" />
						
							<g:sortableColumn property="dtStored" title="${message(code: 'gftMsgB.dtStored.label', default: 'Dt Stored')}" />
						
							<g:sortableColumn property="message" title="${message(code: 'gftMsgB.message.label', default: 'Message')}" />
						
							<g:sortableColumn property="createDt" title="${message(code: 'gftMsgB.createDt.label', default: 'Create Dt')}" />
						
							<g:sortableColumn property="msgId" title="${message(code: 'gftMsgB.msgId.label', default: 'Msg Id')}" />
						
							<th class="header"><g:message code="gftMsgB.rwo_status.label" default="Rwostatus" /></th>
						
							<th class="pull-right">Operations</th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${gftMsgBInstanceList}" var="gftMsgBInstance">
						<tr>
						
							<td>${fieldValue(bean: gftMsgBInstance, field: "direction")}</td>
						
							<td>${fieldValue(bean: gftMsgBInstance, field: "dtStored")}</td>
						
							<td>${fieldValue(bean: gftMsgBInstance, field: "message")}</td>
						
							<td><g:formatDate date="${gftMsgBInstance.createDt}" /></td>
						
							<td>${fieldValue(bean: gftMsgBInstance, field: "msgId")}</td>
						
							<td>${fieldValue(bean: gftMsgBInstance, field: "rwo_status")}</td>
						
							<td class="link text-right intro-entity-button-functions">
									<g:link action="show"
										id="${gftMsgBInstance.id}" class="btn btn-success btn-mini intro-entity-button-show">
										<i class="icon-search icon-white"></i><span class="hidden-phone"> Show</span>
									</g:link>
							</td>
						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-centered">
					<bootstrap:paginate total="${gftMsgBInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>