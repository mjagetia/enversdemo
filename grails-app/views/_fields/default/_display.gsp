<%@ page defaultCodec="html" %>
<div class="control-group">
	<label class="control-label" for="${property}">${label}</label>
	<div class="controls primary-key-id">
		${value}
	</div>
</div>