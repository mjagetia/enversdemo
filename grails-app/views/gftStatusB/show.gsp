
<%@ page import="com.unionbank.rwo.management.GftStatusB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'gftStatusB.label', default: 'GftStatusB')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">

				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.show.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl class="block-800px center">
				
					<g:if test="${gftStatusBInstance?.errorCd}">
						<dt><g:message code="gftStatusB.errorCd.label" default="Error Cd" /></dt>
						
							<dd><g:fieldValue bean="${gftStatusBInstance}" field="errorCd"/></dd>
						
					</g:if>
				
					<g:if test="${gftStatusBInstance?.errorMsg}">
						<dt><g:message code="gftStatusB.errorMsg.label" default="Error Msg" /></dt>
						
							<dd><g:fieldValue bean="${gftStatusBInstance}" field="errorMsg"/></dd>
						
					</g:if>
				
				</dl>

				<g:form class="block-800px center">
					<g:hiddenField name="id" value="${gftStatusBInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn  btn-primary" action="edit" id="${gftStatusBInstance?.id}">
							<i class="icon-pencil icon-white"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<g:form class="form-horizontal" action="edit" id="${gftStatusBInstance?.id}" >
							<g:hiddenField name="version" value="${gftStatusBInstance?.version}" />
							<button type="submit" class="btn btn-danger" name="_action_delete" formnovalidate onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
								<i class="icon-trash icon-white"></i>
								<g:message code="default.button.delete.label" default="Delete" />
							</button>							
						</g:form>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
