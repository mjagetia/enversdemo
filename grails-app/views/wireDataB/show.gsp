
<%@ page import="com.unionbank.rwo.management.WireDataB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'wireDataB.label', default: 'WireDataB')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">

				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.show.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl class="block-800px center">
				
					<g:if test="${wireDataBInstance?.sequenceNbr}">
						<dt><g:message code="wireDataB.sequenceNbr.label" default="Sequence Nbr" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="sequenceNbr"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.cifNbr}">
						<dt><g:message code="wireDataB.cifNbr.label" default="Cif Nbr" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="cifNbr"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.ssoId}">
						<dt><g:message code="wireDataB.ssoId.label" default="Sso Id" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="ssoId"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.sendAccountNumber}">
						<dt><g:message code="wireDataB.sendAccountNumber.label" default="Send Account Number" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="sendAccountNumber"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.sendAccountCode}">
						<dt><g:message code="wireDataB.sendAccountCode.label" default="Send Account Code" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="sendAccountCode"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.sendAccountDesc}">
						<dt><g:message code="wireDataB.sendAccountDesc.label" default="Send Account Desc" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="sendAccountDesc"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.channelType}">
						<dt><g:message code="wireDataB.channelType.label" default="Channel Type" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="channelType"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.trnsfrAmt}">
						<dt><g:message code="wireDataB.trnsfrAmt.label" default="Trnsfr Amt" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="trnsfrAmt"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.transactionFee}">
						<dt><g:message code="wireDataB.transactionFee.label" default="Transaction Fee" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="transactionFee"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.deliveryDt}">
						<dt><g:message code="wireDataB.deliveryDt.label" default="Delivery Dt" /></dt>
						
							<dd><g:formatDate date="${wireDataBInstance?.deliveryDt}" /></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.gftResponseDt}">
						<dt><g:message code="wireDataB.gftResponseDt.label" default="Gft Response Dt" /></dt>
						
							<dd><g:formatDate date="${wireDataBInstance?.gftResponseDt}" /></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.obiComment}">
						<dt><g:message code="wireDataB.obiComment.label" default="Obi Comment" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="obiComment"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.purposeOfWire}">
						<dt><g:message code="wireDataB.purposeOfWire.label" default="Purpose Of Wire" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="purposeOfWire"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.gftTransId}">
						<dt><g:message code="wireDataB.gftTransId.label" default="Gft Trans Id" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="gftTransId"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.bank_Info}">
						<dt><g:message code="wireDataB.bank_Info.label" default="Bank Info" /></dt>
						
							<dd><g:link controller="bankInfoB" action="show" id="${wireDataBInstance?.bank_Info?.id}">${wireDataBInstance?.bank_Info?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.bnfciry}">
						<dt><g:message code="wireDataB.bnfciry.label" default="Bnfciry" /></dt>
						
							<dd><g:link controller="beneficiaryB" action="show" id="${wireDataBInstance?.bnfciry?.id}">${wireDataBInstance?.bnfciry?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.gftMsgBs}">
						<dt><g:message code="wireDataB.gftMsgBs.label" default="Gft Msg Bs" /></dt>
						
							<g:each in="${wireDataBInstance.gftMsgBs}" var="g">
							<dd><g:link controller="gftMsgB" action="show" id="${g.id}">${g?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.status}">
						<dt><g:message code="wireDataB.status.label" default="Status" /></dt>
						
							<dd><g:link controller="statusB" action="show" id="${wireDataBInstance?.status?.id}">${wireDataBInstance?.status?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.transactionId}">
						<dt><g:message code="wireDataB.transactionId.label" default="Transaction Id" /></dt>
						
							<dd><g:fieldValue bean="${wireDataBInstance}" field="transactionId"/></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.trnsfrDt}">
						<dt><g:message code="wireDataB.trnsfrDt.label" default="Trnsfr Dt" /></dt>
						
							<dd><g:formatDate date="${wireDataBInstance?.trnsfrDt}" /></dd>
						
					</g:if>
				
					<g:if test="${wireDataBInstance?.trnsfrUpdtDt}">
						<dt><g:message code="wireDataB.trnsfrUpdtDt.label" default="Trnsfr Updt Dt" /></dt>
						
							<dd><g:formatDate date="${wireDataBInstance?.trnsfrUpdtDt}" /></dd>
						
					</g:if>
				
				</dl>

			<g:form class="block-800px center">
				<g:hiddenField name="id" value="${beneficiaryBInstance?.id}" />
				<div class="form-actions">
					<div class="block-800px center">
						<g:link class="list btn" action="list">
							<i class="icon-arrow-left"></i>
							<g:message code="default.button.back.label" default="Back" />
						</g:link>
					</div>
				</div>
			</g:form>

		</div>

		</div>
	</body>
</html>
