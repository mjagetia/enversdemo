
<%@ page import="com.unionbank.rwo.management.WireDataB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'wireDataB.label', default: 'WireDataB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.list.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped table-800px center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">
						
							<g:sortableColumn property="sequenceNbr" title="${message(code: 'wireDataB.sequenceNbr.label', default: 'Sequence Nbr')}" />
						
							<g:sortableColumn property="cifNbr" title="${message(code: 'wireDataB.cifNbr.label', default: 'Cif Nbr')}" />
						
							<g:sortableColumn property="ssoId" title="${message(code: 'wireDataB.ssoId.label', default: 'Sso Id')}" />
						
							<g:sortableColumn property="sendAccountNumber" title="${message(code: 'wireDataB.sendAccountNumber.label', default: 'Send Account Number')}" />
						
							<g:sortableColumn property="sendAccountCode" title="${message(code: 'wireDataB.sendAccountCode.label', default: 'Send Account Code')}" />
						
							<g:sortableColumn property="sendAccountDesc" title="${message(code: 'wireDataB.sendAccountDesc.label', default: 'Send Account Desc')}" />
						
							<th class="pull-right">Operations</th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${wireDataBInstanceList}" var="wireDataBInstance">
						<tr>
						
							<td>${fieldValue(bean: wireDataBInstance, field: "sequenceNbr")}</td>
						
							<td>${fieldValue(bean: wireDataBInstance, field: "cifNbr")}</td>
						
							<td>${fieldValue(bean: wireDataBInstance, field: "ssoId")}</td>
						
							<td>${fieldValue(bean: wireDataBInstance, field: "sendAccountNumber")}</td>
						
							<td>${fieldValue(bean: wireDataBInstance, field: "sendAccountCode")}</td>
						
							<td>${fieldValue(bean: wireDataBInstance, field: "sendAccountDesc")}</td>
						
							<td class="link text-right intro-entity-button-functions">
									<g:link action="show"
										id="${wireDataBInstance.id}" class="btn btn-success btn-mini intro-entity-button-show">
										<i class="icon-search icon-white"></i><span class="hidden-phone"> Show</span>
									</g:link>
							</td>
						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-centered">
					<bootstrap:paginate total="${wireDataBInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>