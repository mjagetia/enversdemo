
<%@ page import="com.unionbank.rwo.management.WireDataB" %>

<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'wireDataB.label', default: 'WireDataB')}" />
		<title><g:message code="default.revisions.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.revisions.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn" id="entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">

			            <g:sortableColumn property="revisionEntity.id" title="${message(code: 'envers.id.label', default: 'Rev id')}"/>
			
			            <g:sortableColumn property="revisionType" title="${message(code: 'envers.revisionType.label', default: 'Rev type')}"/>
			
			            <g:sortableColumn property="revisionEntity.revisionDate" title="${message(code: 'envers.revisionDate.label', default: 'Rev date')}"/>
			
			            <g:sortableColumn property="revisionEntity.currentUser" title="${message(code: 'envers.currentUser.label', default: 'Rev author')}"/>
						
						
						
						
							<g:sortableColumn property="sequenceNbr" title="${message(code: 'wireDataB.sequenceNbr.label', default: 'Sequence Nbr')}" />
						
							<g:sortableColumn property="cifNbr" title="${message(code: 'wireDataB.cifNbr.label', default: 'Cif Nbr')}" />
						
							<g:sortableColumn property="ssoId" title="${message(code: 'wireDataB.ssoId.label', default: 'Sso Id')}" />
						
							<g:sortableColumn property="sendAccountNumber" title="${message(code: 'wireDataB.sendAccountNumber.label', default: 'Send Account Number')}" />
						
							<g:sortableColumn property="sendAccountCode" title="${message(code: 'wireDataB.sendAccountCode.label', default: 'Send Account Code')}" />
						
							<g:sortableColumn property="sendAccountDesc" title="${message(code: 'wireDataB.sendAccountDesc.label', default: 'Send Account Desc')}" />
						
						</tr>
					</thead>
					<tbody>
					<g:each in="${wireDataBRevisionInstanceList}" var="wireDataBRevision">
						<tr>
					<td><g:fieldValue field="revisionEntity.id" bean="${wireDataBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionType" bean="${wireDataBRevision}" /></td>
						
			                <td><g:fieldValue field="revisionEntity.revisionDate" bean="${wireDataBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionEntity.currentUser" bean="${wireDataBRevision}" /></td>

						
							<td>${fieldValue(bean: wireDataBRevision, field: "sequenceNbr")}</td>
						
							<td>${fieldValue(bean: wireDataBRevision, field: "cifNbr")}</td>
						
							<td>${fieldValue(bean: wireDataBRevision, field: "ssoId")}</td>
						
							<td>${fieldValue(bean: wireDataBRevision, field: "sendAccountNumber")}</td>
						
							<td>${fieldValue(bean: wireDataBRevision, field: "sendAccountCode")}</td>
						
							<td>${fieldValue(bean: wireDataBRevision, field: "sendAccountDesc")}</td>
						

						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-right">
					<bootstrap:paginate total="${wireDataBRevisionInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
