<html>
<head>
	<meta name='layout' content='bootstrap'/>
	<title><g:message code="springSecurity.login.title"/></title>
</head>

<body>
<div id='login'>
	<div class='inner'>
		<g:if test='${flash.message}'>
		<div class="alert block-800px center">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  			${flash.message}
		</div>
		</g:if>



			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="panel panel-default">
						<div class="login-ub-logo">
						    <span>UnionBank Logo</span>
						  </div>
							<div class="panel-heading">
								<h3 class="panel-title"><g:message code="springSecurity.login.header"/></h3>
							</div>
							<div class="panel-body">
								<form accept-charset="UTF-8" role="form"  action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
									<fieldset>
										<div class="form-group">
											<input class="text_ form-control" placeholder="<g:message code="springSecurity.login.username.label" default="UB network username" />"
												type='text' name='j_username' id='username'/>
										</div>
										<div class="form-group">
											<input class="text_ form-control" placeholder="<g:message code="springSecurity.login.password.label"/>"
												value=""  type="password" name="j_password" id="password" />
										</div>
										%{--
										<div class="checkbox">
											<label>
												<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
											 	<g:message code="springSecurity.login.remember.me.label"/>
											</label>
										</div>
										--}%
										<input class="btn btn-lg btn-success btn-block" type="submit"
											value="Login">
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>			


		</div>
</div>
<script type='text/javascript'>
	<!--
	(function() {
		// TODO: issues with placeholder on ie 8
//		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
<g:javascript src="jquery.placeholder.js" />
</body>
</html>
