
<%@ page import="com.unionbank.rwo.management.BankInfoB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'bankInfoB.label', default: 'BankInfoB')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">

				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.show.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl class="block-800px center">
				
					<g:if test="${bankInfoBInstance?.abaNbr}">
						<dt><g:message code="bankInfoB.abaNbr.label" default="Aba Nbr" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="abaNbr"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.bankName}">
						<dt><g:message code="bankInfoB.bankName.label" default="Bank Name" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="bankName"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.addrLineOne}">
						<dt><g:message code="bankInfoB.addrLineOne.label" default="Addr Line One" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="addrLineOne"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.addrLineTwo}">
						<dt><g:message code="bankInfoB.addrLineTwo.label" default="Addr Line Two" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="addrLineTwo"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.addrLineThree}">
						<dt><g:message code="bankInfoB.addrLineThree.label" default="Addr Line Three" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="addrLineThree"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.city}">
						<dt><g:message code="bankInfoB.city.label" default="City" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="city"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.state}">
						<dt><g:message code="bankInfoB.state.label" default="State" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="state"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.postalCd}">
						<dt><g:message code="bankInfoB.postalCd.label" default="Postal Cd" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="postalCd"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.country}">
						<dt><g:message code="bankInfoB.country.label" default="Country" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="country"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.entityRole}">
						<dt><g:message code="bankInfoB.entityRole.label" default="Entity Role" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="entityRole"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.bankInfoId}">
						<dt><g:message code="bankInfoB.bankInfoId.label" default="Bank Info Id" /></dt>
						
							<dd><g:fieldValue bean="${bankInfoBInstance}" field="bankInfoId"/></dd>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.beneficiaryBs}">
						<dt><g:message code="bankInfoB.beneficiaryBs.label" default="Beneficiary Bs" /></dt>
						
							<g:each in="${bankInfoBInstance.beneficiaryBs}" var="b">
							<dd><g:link controller="beneficiaryB" action="show" id="${b.id}">${b?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
					<g:if test="${bankInfoBInstance?.wireDataBs}">
						<dt><g:message code="bankInfoB.wireDataBs.label" default="Wire Data Bs" /></dt>
						
							<g:each in="${bankInfoBInstance.wireDataBs}" var="w">
							<dd><g:link controller="wireDataB" action="show" id="${w.id}">${w?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
				</dl>

				<g:form class="block-800px center">
					<g:hiddenField name="id" value="${bankInfoBInstance?.id}" />
					<div class="form-actions">
					<div class="block-800px center">
						<g:link class="list btn" action="list">
							<i class="icon-arrow-left"></i>
							<g:message code="default.button.back.label" default="Back" />
						</g:link>
					</div>
				</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
