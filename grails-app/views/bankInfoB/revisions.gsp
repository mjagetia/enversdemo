
<%@ page import="com.unionbank.rwo.management.BankInfoB" %>

<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'bankInfoB.label', default: 'BankInfoB')}" />
		<title><g:message code="default.revisions.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.revisions.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn" id="entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">

			            <g:sortableColumn property="revisionEntity.id" title="${message(code: 'envers.id.label', default: 'Rev id')}"/>
			
			            <g:sortableColumn property="revisionType" title="${message(code: 'envers.revisionType.label', default: 'Rev type')}"/>
			
			            <g:sortableColumn property="revisionEntity.revisionDate" title="${message(code: 'envers.revisionDate.label', default: 'Rev date')}"/>
			
			            <g:sortableColumn property="revisionEntity.currentUser" title="${message(code: 'envers.currentUser.label', default: 'Rev author')}"/>
						
						
						
						
							<g:sortableColumn property="abaNbr" title="${message(code: 'bankInfoB.abaNbr.label', default: 'Aba Nbr')}" />
						
							<g:sortableColumn property="bankName" title="${message(code: 'bankInfoB.bankName.label', default: 'Bank Name')}" />
						
							<g:sortableColumn property="addrLineOne" title="${message(code: 'bankInfoB.addrLineOne.label', default: 'Addr Line One')}" />
						
							<g:sortableColumn property="addrLineTwo" title="${message(code: 'bankInfoB.addrLineTwo.label', default: 'Addr Line Two')}" />
						
							<g:sortableColumn property="addrLineThree" title="${message(code: 'bankInfoB.addrLineThree.label', default: 'Addr Line Three')}" />
						
							<g:sortableColumn property="city" title="${message(code: 'bankInfoB.city.label', default: 'City')}" />
						
						</tr>
					</thead>
					<tbody>
					<g:each in="${bankInfoBRevisionInstanceList}" var="bankInfoBRevision">
						<tr>
					<td><g:fieldValue field="revisionEntity.id" bean="${bankInfoBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionType" bean="${bankInfoBRevision}" /></td>
						
			                <td><g:fieldValue field="revisionEntity.revisionDate" bean="${bankInfoBRevision}" /></td>
			
			                <td><g:fieldValue field="revisionEntity.currentUser" bean="${bankInfoBRevision}" /></td>

						
							<td>${fieldValue(bean: bankInfoBRevision, field: "abaNbr")}</td>
						
							<td>${fieldValue(bean: bankInfoBRevision, field: "bankName")}</td>
						
							<td>${fieldValue(bean: bankInfoBRevision, field: "addrLineOne")}</td>
						
							<td>${fieldValue(bean: bankInfoBRevision, field: "addrLineTwo")}</td>
						
							<td>${fieldValue(bean: bankInfoBRevision, field: "addrLineThree")}</td>
						
							<td>${fieldValue(bean: bankInfoBRevision, field: "city")}</td>
						

						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-right">
					<bootstrap:paginate total="${bankInfoBRevisionInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
