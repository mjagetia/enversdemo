
<%@ page import="com.unionbank.rwo.management.BankInfoB" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'bankInfoB.label', default: 'BankInfoB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.list.label" args=" " /></span> ${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> ${entityName}</span>
						</g:link>
						<g:if test="${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
					</div>
				</div>				

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped table-800px center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">
						
							<g:sortableColumn property="abaNbr" title="${message(code: 'bankInfoB.abaNbr.label', default: 'Aba Nbr')}" />
						
							<g:sortableColumn property="bankName" title="${message(code: 'bankInfoB.bankName.label', default: 'Bank Name')}" />
						
							<g:sortableColumn property="addrLineOne" title="${message(code: 'bankInfoB.addrLineOne.label', default: 'Addr Line One')}" />
						
							<g:sortableColumn property="addrLineTwo" title="${message(code: 'bankInfoB.addrLineTwo.label', default: 'Addr Line Two')}" />
						
							<g:sortableColumn property="addrLineThree" title="${message(code: 'bankInfoB.addrLineThree.label', default: 'Addr Line Three')}" />
						
							<g:sortableColumn property="city" title="${message(code: 'bankInfoB.city.label', default: 'City')}" />
						
							<th class="pull-right">Operations</th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${bankInfoBInstanceList}" var="bankInfoBInstance">
						<tr>
						
							<td>${fieldValue(bean: bankInfoBInstance, field: "abaNbr")}</td>
						
							<td>${fieldValue(bean: bankInfoBInstance, field: "bankName")}</td>
						
							<td>${fieldValue(bean: bankInfoBInstance, field: "addrLineOne")}</td>
						
							<td>${fieldValue(bean: bankInfoBInstance, field: "addrLineTwo")}</td>
						
							<td>${fieldValue(bean: bankInfoBInstance, field: "addrLineThree")}</td>
						
							<td>${fieldValue(bean: bankInfoBInstance, field: "city")}</td>
						
							<td class="link text-right intro-entity-button-functions">
									<g:link action="show"
										id="${bankInfoBInstance.id}" class="btn btn-success btn-mini intro-entity-button-show">
										<i class="icon-search icon-white"></i><span class="hidden-phone"> Show</span>
									</g:link>
							</td>
						</tr>
						
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-centered">
					<bootstrap:paginate total="${bankInfoBInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>