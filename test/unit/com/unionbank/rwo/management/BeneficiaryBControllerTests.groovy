package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(BeneficiaryBController)
@Mock(BeneficiaryB)
class BeneficiaryBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/beneficiaryB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.beneficiaryBInstanceList.size() == 0
        assert model.beneficiaryBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.beneficiaryBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.beneficiaryBInstance != null
        assert view == '/beneficiaryB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/beneficiaryB/show/1'
        assert controller.flash.message != null
        assert BeneficiaryB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/beneficiaryB/list'

        populateValidParams(params)
        def beneficiaryB = new BeneficiaryB(params)

        assert beneficiaryB.save() != null

        params.id = beneficiaryB.id

        def model = controller.show()

        assert model.beneficiaryBInstance == beneficiaryB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/beneficiaryB/list'

        populateValidParams(params)
        def beneficiaryB = new BeneficiaryB(params)

        assert beneficiaryB.save() != null

        params.id = beneficiaryB.id

        def model = controller.edit()

        assert model.beneficiaryBInstance == beneficiaryB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/beneficiaryB/list'

        response.reset()

        populateValidParams(params)
        def beneficiaryB = new BeneficiaryB(params)

        assert beneficiaryB.save() != null

        // test invalid parameters in update
        params.id = beneficiaryB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/beneficiaryB/edit"
        assert model.beneficiaryBInstance != null

        beneficiaryB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/beneficiaryB/show/$beneficiaryB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        beneficiaryB.clearErrors()

        populateValidParams(params)
        params.id = beneficiaryB.id
        params.version = -1
        controller.update()

        assert view == "/beneficiaryB/edit"
        assert model.beneficiaryBInstance != null
        assert model.beneficiaryBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/beneficiaryB/list'

        response.reset()

        populateValidParams(params)
        def beneficiaryB = new BeneficiaryB(params)

        assert beneficiaryB.save() != null
        assert BeneficiaryB.count() == 1

        params.id = beneficiaryB.id

        controller.delete()

        assert BeneficiaryB.count() == 0
        assert BeneficiaryB.get(beneficiaryB.id) == null
        assert response.redirectedUrl == '/beneficiaryB/list'
    }
}
