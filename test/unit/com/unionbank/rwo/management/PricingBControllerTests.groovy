package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(PricingBController)
@Mock(PricingB)
class PricingBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/pricingB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.pricingBInstanceList.size() == 0
        assert model.pricingBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.pricingBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.pricingBInstance != null
        assert view == '/pricingB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/pricingB/show/1'
        assert controller.flash.message != null
        assert PricingB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/pricingB/list'

        populateValidParams(params)
        def pricingB = new PricingB(params)

        assert pricingB.save() != null

        params.id = pricingB.id

        def model = controller.show()

        assert model.pricingBInstance == pricingB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/pricingB/list'

        populateValidParams(params)
        def pricingB = new PricingB(params)

        assert pricingB.save() != null

        params.id = pricingB.id

        def model = controller.edit()

        assert model.pricingBInstance == pricingB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/pricingB/list'

        response.reset()

        populateValidParams(params)
        def pricingB = new PricingB(params)

        assert pricingB.save() != null

        // test invalid parameters in update
        params.id = pricingB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/pricingB/edit"
        assert model.pricingBInstance != null

        pricingB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/pricingB/show/$pricingB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        pricingB.clearErrors()

        populateValidParams(params)
        params.id = pricingB.id
        params.version = -1
        controller.update()

        assert view == "/pricingB/edit"
        assert model.pricingBInstance != null
        assert model.pricingBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/pricingB/list'

        response.reset()

        populateValidParams(params)
        def pricingB = new PricingB(params)

        assert pricingB.save() != null
        assert PricingB.count() == 1

        params.id = pricingB.id

        controller.delete()

        assert PricingB.count() == 0
        assert PricingB.get(pricingB.id) == null
        assert response.redirectedUrl == '/pricingB/list'
    }
}
