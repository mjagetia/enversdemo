package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(WeeklyLimitBController)
@Mock(WeeklyLimitB)
class WeeklyLimitBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/weeklyLimitB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.weeklyLimitBInstanceList.size() == 0
        assert model.weeklyLimitBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.weeklyLimitBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.weeklyLimitBInstance != null
        assert view == '/weeklyLimitB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/weeklyLimitB/show/1'
        assert controller.flash.message != null
        assert WeeklyLimitB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/weeklyLimitB/list'

        populateValidParams(params)
        def weeklyLimitB = new WeeklyLimitB(params)

        assert weeklyLimitB.save() != null

        params.id = weeklyLimitB.id

        def model = controller.show()

        assert model.weeklyLimitBInstance == weeklyLimitB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/weeklyLimitB/list'

        populateValidParams(params)
        def weeklyLimitB = new WeeklyLimitB(params)

        assert weeklyLimitB.save() != null

        params.id = weeklyLimitB.id

        def model = controller.edit()

        assert model.weeklyLimitBInstance == weeklyLimitB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/weeklyLimitB/list'

        response.reset()

        populateValidParams(params)
        def weeklyLimitB = new WeeklyLimitB(params)

        assert weeklyLimitB.save() != null

        // test invalid parameters in update
        params.id = weeklyLimitB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/weeklyLimitB/edit"
        assert model.weeklyLimitBInstance != null

        weeklyLimitB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/weeklyLimitB/show/$weeklyLimitB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        weeklyLimitB.clearErrors()

        populateValidParams(params)
        params.id = weeklyLimitB.id
        params.version = -1
        controller.update()

        assert view == "/weeklyLimitB/edit"
        assert model.weeklyLimitBInstance != null
        assert model.weeklyLimitBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/weeklyLimitB/list'

        response.reset()

        populateValidParams(params)
        def weeklyLimitB = new WeeklyLimitB(params)

        assert weeklyLimitB.save() != null
        assert WeeklyLimitB.count() == 1

        params.id = weeklyLimitB.id

        controller.delete()

        assert WeeklyLimitB.count() == 0
        assert WeeklyLimitB.get(weeklyLimitB.id) == null
        assert response.redirectedUrl == '/weeklyLimitB/list'
    }
}
