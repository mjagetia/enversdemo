package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(GftStatusBController)
@Mock(GftStatusB)
class GftStatusBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/gftStatusB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.gftStatusBInstanceList.size() == 0
        assert model.gftStatusBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.gftStatusBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.gftStatusBInstance != null
        assert view == '/gftStatusB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/gftStatusB/show/1'
        assert controller.flash.message != null
        assert GftStatusB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/gftStatusB/list'

        populateValidParams(params)
        def gftStatusB = new GftStatusB(params)

        assert gftStatusB.save() != null

        params.id = gftStatusB.id

        def model = controller.show()

        assert model.gftStatusBInstance == gftStatusB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/gftStatusB/list'

        populateValidParams(params)
        def gftStatusB = new GftStatusB(params)

        assert gftStatusB.save() != null

        params.id = gftStatusB.id

        def model = controller.edit()

        assert model.gftStatusBInstance == gftStatusB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/gftStatusB/list'

        response.reset()

        populateValidParams(params)
        def gftStatusB = new GftStatusB(params)

        assert gftStatusB.save() != null

        // test invalid parameters in update
        params.id = gftStatusB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/gftStatusB/edit"
        assert model.gftStatusBInstance != null

        gftStatusB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/gftStatusB/show/$gftStatusB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        gftStatusB.clearErrors()

        populateValidParams(params)
        params.id = gftStatusB.id
        params.version = -1
        controller.update()

        assert view == "/gftStatusB/edit"
        assert model.gftStatusBInstance != null
        assert model.gftStatusBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/gftStatusB/list'

        response.reset()

        populateValidParams(params)
        def gftStatusB = new GftStatusB(params)

        assert gftStatusB.save() != null
        assert GftStatusB.count() == 1

        params.id = gftStatusB.id

        controller.delete()

        assert GftStatusB.count() == 0
        assert GftStatusB.get(gftStatusB.id) == null
        assert response.redirectedUrl == '/gftStatusB/list'
    }
}
