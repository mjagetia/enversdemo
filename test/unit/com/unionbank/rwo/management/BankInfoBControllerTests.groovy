package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(BankInfoBController)
@Mock(BankInfoB)
class BankInfoBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/bankInfoB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.bankInfoBInstanceList.size() == 0
        assert model.bankInfoBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.bankInfoBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.bankInfoBInstance != null
        assert view == '/bankInfoB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/bankInfoB/show/1'
        assert controller.flash.message != null
        assert BankInfoB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/bankInfoB/list'

        populateValidParams(params)
        def bankInfoB = new BankInfoB(params)

        assert bankInfoB.save() != null

        params.id = bankInfoB.id

        def model = controller.show()

        assert model.bankInfoBInstance == bankInfoB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/bankInfoB/list'

        populateValidParams(params)
        def bankInfoB = new BankInfoB(params)

        assert bankInfoB.save() != null

        params.id = bankInfoB.id

        def model = controller.edit()

        assert model.bankInfoBInstance == bankInfoB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/bankInfoB/list'

        response.reset()

        populateValidParams(params)
        def bankInfoB = new BankInfoB(params)

        assert bankInfoB.save() != null

        // test invalid parameters in update
        params.id = bankInfoB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/bankInfoB/edit"
        assert model.bankInfoBInstance != null

        bankInfoB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/bankInfoB/show/$bankInfoB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        bankInfoB.clearErrors()

        populateValidParams(params)
        params.id = bankInfoB.id
        params.version = -1
        controller.update()

        assert view == "/bankInfoB/edit"
        assert model.bankInfoBInstance != null
        assert model.bankInfoBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/bankInfoB/list'

        response.reset()

        populateValidParams(params)
        def bankInfoB = new BankInfoB(params)

        assert bankInfoB.save() != null
        assert BankInfoB.count() == 1

        params.id = bankInfoB.id

        controller.delete()

        assert BankInfoB.count() == 0
        assert BankInfoB.get(bankInfoB.id) == null
        assert response.redirectedUrl == '/bankInfoB/list'
    }
}
