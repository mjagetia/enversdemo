package com.unionbank.rwo.management



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(SystemDataBService)
class SystemDataBServiceSpec {

    void testSomething() {
        fail "Implement me"
    }
}
