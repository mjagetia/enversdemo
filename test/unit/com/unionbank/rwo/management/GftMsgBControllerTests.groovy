package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(GftMsgBController)
@Mock(GftMsgB)
class GftMsgBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/gftMsgB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.gftMsgBInstanceList.size() == 0
        assert model.gftMsgBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.gftMsgBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.gftMsgBInstance != null
        assert view == '/gftMsgB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/gftMsgB/show/1'
        assert controller.flash.message != null
        assert GftMsgB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/gftMsgB/list'

        populateValidParams(params)
        def gftMsgB = new GftMsgB(params)

        assert gftMsgB.save() != null

        params.id = gftMsgB.id

        def model = controller.show()

        assert model.gftMsgBInstance == gftMsgB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/gftMsgB/list'

        populateValidParams(params)
        def gftMsgB = new GftMsgB(params)

        assert gftMsgB.save() != null

        params.id = gftMsgB.id

        def model = controller.edit()

        assert model.gftMsgBInstance == gftMsgB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/gftMsgB/list'

        response.reset()

        populateValidParams(params)
        def gftMsgB = new GftMsgB(params)

        assert gftMsgB.save() != null

        // test invalid parameters in update
        params.id = gftMsgB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/gftMsgB/edit"
        assert model.gftMsgBInstance != null

        gftMsgB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/gftMsgB/show/$gftMsgB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        gftMsgB.clearErrors()

        populateValidParams(params)
        params.id = gftMsgB.id
        params.version = -1
        controller.update()

        assert view == "/gftMsgB/edit"
        assert model.gftMsgBInstance != null
        assert model.gftMsgBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/gftMsgB/list'

        response.reset()

        populateValidParams(params)
        def gftMsgB = new GftMsgB(params)

        assert gftMsgB.save() != null
        assert GftMsgB.count() == 1

        params.id = gftMsgB.id

        controller.delete()

        assert GftMsgB.count() == 0
        assert GftMsgB.get(gftMsgB.id) == null
        assert response.redirectedUrl == '/gftMsgB/list'
    }
}
