package com.unionbank.rwo.management



import org.junit.*
import grails.test.mixin.*

@TestFor(WireDataBController)
@Mock(WireDataB)
class WireDataBControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/wireDataB/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.wireDataBInstanceList.size() == 0
        assert model.wireDataBInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.wireDataBInstance != null
    }

    void testSave() {
        controller.save()

        assert model.wireDataBInstance != null
        assert view == '/wireDataB/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/wireDataB/show/1'
        assert controller.flash.message != null
        assert WireDataB.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/wireDataB/list'

        populateValidParams(params)
        def wireDataB = new WireDataB(params)

        assert wireDataB.save() != null

        params.id = wireDataB.id

        def model = controller.show()

        assert model.wireDataBInstance == wireDataB
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/wireDataB/list'

        populateValidParams(params)
        def wireDataB = new WireDataB(params)

        assert wireDataB.save() != null

        params.id = wireDataB.id

        def model = controller.edit()

        assert model.wireDataBInstance == wireDataB
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/wireDataB/list'

        response.reset()

        populateValidParams(params)
        def wireDataB = new WireDataB(params)

        assert wireDataB.save() != null

        // test invalid parameters in update
        params.id = wireDataB.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/wireDataB/edit"
        assert model.wireDataBInstance != null

        wireDataB.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/wireDataB/show/$wireDataB.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        wireDataB.clearErrors()

        populateValidParams(params)
        params.id = wireDataB.id
        params.version = -1
        controller.update()

        assert view == "/wireDataB/edit"
        assert model.wireDataBInstance != null
        assert model.wireDataBInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/wireDataB/list'

        response.reset()

        populateValidParams(params)
        def wireDataB = new WireDataB(params)

        assert wireDataB.save() != null
        assert WireDataB.count() == 1

        params.id = wireDataB.id

        controller.delete()

        assert WireDataB.count() == 0
        assert WireDataB.get(wireDataB.id) == null
        assert response.redirectedUrl == '/wireDataB/list'
    }
}
