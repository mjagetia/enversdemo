package com.unionbank.auth.ldap

import org.springframework.security.core.userdetails.User

class LdapUserDetails extends User{
    // extra instance variables final String fullname final String email final String title
    String fullname
    String email
    String title
    String phone
	String username

    String photo

    LdapUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
                  boolean credentialsNonExpired, boolean accountNonLocked, Collection authorities,
                  String fullname, String email, String title, String photo, String phone) {

        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities)
				  
				  this.username = username

        this.fullname = fullname
        this.email = email
        this.title = title
        this.photo = photo
        this.phone = phone
    }
}