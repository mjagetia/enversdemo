package com.unionbank.auth.ldap


class User {

//	transient springSecurityService

	String username
	boolean enabled

	static constraints = {
		username blank: false, unique: true
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

}
