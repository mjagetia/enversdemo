package com.unionbank.rwo.utils

class UBGrailsClassUtils {
	public static boolean isClassBelowPackage(Class<?> theClass, List<?> packageList) {
		if(theClass.package==null)return false
		String classPackage = theClass.package.name;
		for (Object packageName : packageList) {
			if (packageName != null) {
				if (classPackage.startsWith(packageName.toString())) {
					return true;
				}
			}
		}
		return false;
	}
}
