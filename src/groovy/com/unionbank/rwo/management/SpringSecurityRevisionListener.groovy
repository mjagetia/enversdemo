package com.unionbank.rwo.management

import com.unionbank.auth.ldap.LdapUserDetails
import org.hibernate.envers.RevisionListener

class SpringSecurityRevisionListener implements RevisionListener {

    public void newRevision(Object entity) {
		def springSecurityService = SpringSecurityServiceHolder.springSecurityService
        UserRevisionEntity revisionEntity = (UserRevisionEntity) entity
		LdapUserDetails user = springSecurityService?.getPrincipal()
        revisionEntity.currentUser = user.username
    }
}