<%=packageName ? "package ${packageName}\n\n" : ''%>import org.springframework.dao.DataIntegrityViolationException

class ${className}Service {
	static transactional = true
	
	def save(Map<Object, Object> params) {
		def ${propertyName} = new ${className}(params)
			if (!${propertyName}.save(flush: true)) {
				throw new GenericSaveException();
			}
		return ${propertyName}
	}
	
	def create(Map<Object, Object> params) {
		return new ${className}(params)
	}

	def edit(String id) {
		def ${propertyName} = ${className}.get(id)
		if (!${propertyName}) {
			throw new GenericEditException();
		}
		return ${propertyName}
	}
	
	def update(String id, Long version, Map<Object, Object> params) {
		def ${propertyName} = ${className}.get(id)
		if (!${propertyName}) {
			throw new GenericNotFoundException();
		}

		if (version != null) {
			if (${propertyName}.version > version) {
				${propertyName}.errors.rejectValue("version", "default.optimistic.locking.failure",
					[message(code: '${domainClass.getLogicalPropertyName()}.label', default: '${className}')] as Object[],
					"Another user has updated this ${className} while you were editing")
				throw new GenericVersionMismatchException();
			}
		}
		${propertyName}.properties = params
		if (!${propertyName}.save(flush: true)) {
			throw new GenericSaveException();
		}
		return ${propertyName}
	}

	def delete(String id) {
		def ${propertyName} = ${className}.get(id)
		if (!${propertyName}) {
			throw new GenericNotFoundException()
		}

		${propertyName}.delete(flush: true)
	}
	
}