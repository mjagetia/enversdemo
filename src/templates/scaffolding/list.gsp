<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.list.label" args=" " /></span> \${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> \${entityName}</span>
						</g:link>
						<g:if test="\${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">\${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">\${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="\${flash.message}">
				<bootstrap:alert class="alert-info block-800px center">\${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped table-800px center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">
						<%  excludedProps = Event.allEvents.toList() << 'version'
							allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
							props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) }
							Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
							props.eachWithIndex { p, i ->
								if (i < 6) {
									if (p.isAssociation()) { %>
							<th class="header"><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></th>
						<%      } else { %>
							<g:sortableColumn property="${p.name}" title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${p.naturalName}')}" />
						<%  }   }   } %>
							<th class="pull-right">Operations</th>
						</tr>
					</thead>
					<tbody>
					<g:each in="\${${propertyName}List}" var="${propertyName}">
						<tr>
						<%  
							def introClass = "intro-entity-button-"
							props.eachWithIndex { p, i ->
						        if (i < 6) {
									if (p.type == Boolean || p.type == boolean) { %>
							<td><g:formatBoolean boolean="\${${propertyName}.${p.name}}" /></td>
						<%          } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
							<td><g:formatDate date="\${${propertyName}.${p.name}}" /></td>
						<%          } else { %>
							<td>\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
						<%  }   }   } %>
							<td class="link text-right ${introClass}functions">
									<g:link action="show"
										id="\${${propertyName}.id}" class="btn btn-success btn-mini ${introClass}show">
										<i class="icon-search icon-white"></i><span class="hidden-phone"> Show</span>
									</g:link>
									 <g:link
										class="btn btn-primary btn-mini ${introClass}edit }" action="edit"
										id="\${${propertyName}?.id}">
										<i class="icon-pencil icon-white"></i><span class="hidden-phone"> <g:message code="default.button.edit.label" default="Edit" /></span>
									</g:link> 
									<g:form class="form-inline display-inline" action="edit" id="\${${propertyName}?.id}" >
										<g:hiddenField name="version" value="\${${propertyName}?.version}" />
										<g:hiddenField name="id" value="\${${propertyName}?.id}" />
										<button type="submit" class="btn btn-danger btn-mini ${introClass}delete" name="_action_delete" formnovalidate onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
											<i class="icon-trash icon-white"></i><span class="hidden-phone"> <g:message code="default.button.delete.label" default="Delete" /></span>
										</button>							
									</g:form>
							</td>
						</tr>
						<% introClass = "" %>
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-centered">
					<bootstrap:paginate total="\${${propertyName}Total}" />
				</div>
			</div>

		</div>
	</body>
</html>