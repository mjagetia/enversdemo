<% import grails.persistence.Event %>
<%=packageName%>
<% def revPropertyName = "${domainClass.getLogicalPropertyName()}Revision" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.revisions.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.revisions.label" args=" " /></span> \${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> \${entityName}</span>
						</g:link>
						<g:if test="\${audited}">
						<g:link class="revisions btn" id="entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">\${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">\${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="\${flash.message}">
				<bootstrap:alert class="alert-info">\${flash.message}</bootstrap:alert>
				</g:if>
				<div class="table-responsive">
				<table class="table table-striped center intro-page-main-data">
					<thead>
						<tr class="intro-page-main-metadata">

			            <g:sortableColumn property="revisionEntity.id" title="\${message(code: 'envers.id.label', default: 'Rev id')}"/>
			
			            <g:sortableColumn property="revisionType" title="\${message(code: 'envers.revisionType.label', default: 'Rev type')}"/>
			
			            <g:sortableColumn property="revisionEntity.revisionDate" title="\${message(code: 'envers.revisionDate.label', default: 'Rev date')}"/>
			
			            <g:sortableColumn property="revisionEntity.currentUser" title="\${message(code: 'envers.currentUser.label', default: 'Rev author')}"/>
						
						
						
						<%  excludedProps = Event.allEvents.toList() << 'version'
							allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
							props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) }
							Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
							props.eachWithIndex { p, i ->
								if (i < 6) {
									if (p.isAssociation()) { %>
							<th class="header"><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></th>
						<%      } else { %>
							<g:sortableColumn property="${p.name}" title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${p.naturalName}')}" />
						<%  }   }   } %>
						</tr>
					</thead>
					<tbody>
					<g:each in="\${${revPropertyName}InstanceList}" var="${revPropertyName}">
						<tr>
					<td><g:fieldValue field="revisionEntity.id" bean="\${${revPropertyName}}" /></td>
			
			                <td><g:fieldValue field="revisionType" bean="\${${revPropertyName}}" /></td>
						
			                <td><g:fieldValue field="revisionEntity.revisionDate" bean="\${${revPropertyName}}" /></td>
			
			                <td><g:fieldValue field="revisionEntity.currentUser" bean="\${${revPropertyName}}" /></td>

						<%  
							def introClass = "intro-entity-button-"
							props.eachWithIndex { p, i ->
						        if (i < 6) {
									if (p.type == Boolean || p.type == boolean) { %>
							<td><g:formatBoolean boolean="\${${revPropertyName}.${p.name}}" /></td>
						<%          } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
							<td><g:formatDate date="\${${revPropertyName}.${p.name}}" /></td>
						<%          } else { %>
							<td>\${fieldValue(bean: ${revPropertyName}, field: "${p.name}")}</td>
						<%  }   }   } %>

						</tr>
						<% introClass = "" %>
					</g:each>
					</tbody>
				</table>
				</div>
				<div class="pagination pagination-right">
					<bootstrap:paginate total="\${${revPropertyName}InstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
