<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">

				<div class="page-header">
					<h1 calss="intro-page-head"><span class="hidden-phone"><g:message code="default.show.label" args=" " /></span> \${entityName}</h1>
					<div class="sub-actions btn-group pull-right intro-page-common-functions-top">
						<a id="intro-testdrive" class="btn btn-inverse intro-testdrive-button" title="Test Drive RWO-A UI"><i class="icon-certificate icon-white"></i></a>
						<g:link class="list btn btn-info intro-entity-list-button" action="list">
							<i class="icon-list icon-white"></i>
							<span class="hidden-phone"><g:message code="default.list.label" args=" "/></span><span class="visible-desktop"> \${entityName}</span>
						</g:link>
						<g:if test="\${audited}">
						<g:link class="revisions btn intro-entity-revisions-button" action="revisions">
							<i class="icon-time"></i>
							<span class="visible-desktop">\${entityName} </span><span class="hidden-phone"><g:message code="default.revisions.label" args=" "/></span>
						</g:link>
						</g:if>
						<g:link class="create btn btn-success intro-entity-create-button" action="create">
							<i class="icon-plus icon-white"></i>
							<span class="hidden-phone"><g:message code="default.create.label" args=" "/> </span><span class="visible-desktop">\${entityName}</span>
						</g:link>
					</div>
				</div>				

				<g:if test="\${flash.message}">
				<bootstrap:alert class="alert-info">\${flash.message}</bootstrap:alert>
				</g:if>

				<dl class="block-800px center">
				<%  excludedProps = Event.allEvents.toList() << 'id' << 'version'
					allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
					props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
					Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
					props.each { p -> %>
					<g:if test="\${${propertyName}?.${p.name}}">
						<dt><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></dt>
						<%  if (p.isEnum()) { %>
							<dd><g:fieldValue bean="\${${propertyName}}" field="${p.name}"/></dd>
						<%  } else if (p.oneToMany || p.manyToMany) { %>
							<g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
							<dd><g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></dd>
							</g:each>
						<%  } else if (p.manyToOne || p.oneToOne) { %>
							<dd><g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></dd>
						<%  } else if (p.type == Boolean || p.type == boolean) { %>
							<dd><g:formatBoolean boolean="\${${propertyName}?.${p.name}}" /></dd>
						<%  } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
							<dd><g:formatDate date="\${${propertyName}?.${p.name}}" /></dd>
						<%  } else if(!p.type.isArray()) { %>
							<dd><g:fieldValue bean="\${${propertyName}}" field="${p.name}"/></dd>
						<%  } %>
					</g:if>
				<%  } %>
				</dl>

				<g:form class="block-800px center">
					<g:hiddenField name="id" value="\${${propertyName}?.id}" />
					<div class="form-actions">
						<g:link class="btn  btn-primary" action="edit" id="\${${propertyName}?.id}">
							<i class="icon-pencil icon-white"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<g:form class="form-horizontal" action="edit" id="\${${propertyName}?.id}" >
							<g:hiddenField name="version" value="\${${propertyName}?.version}" />
							<button type="submit" class="btn btn-danger" name="_action_delete" formnovalidate onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
								<i class="icon-trash icon-white"></i>
								<g:message code="default.button.delete.label" default="Delete" />
							</button>							
						</g:form>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
