<%=packageName ? "package ${packageName}\n\n" : ''%>import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DuplicateKeyException
<% def revPropertyName = "${domainClass.getLogicalPropertyName()}Revision" %>
<%
	def domainId = domainClass.getPropertyByName("id")
	def idType = domainId?.getType()?.getName()
%>
class ${className}Controller {

	def ${domainClass.propertyName}Service

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def revisions = {
		params.max = Math.min(params.int('max') ?: 10, 20)
		params.offset = params.int('offset') ?: 0
		params.order = "desc"
		// TODO: obtaining size should be improved
		def ${revPropertyName}InstanceTotal = ${className}.findAllRevisions().size()
		def audited = ${className}.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		render(view: 'revisions', model: [${revPropertyName}InstanceList: ${className}.findAllRevisions(params).reverse(), ${revPropertyName}InstanceTotal: ${revPropertyName}InstanceTotal, audited: audited])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def audited = ${className}.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [${propertyName}List: ${className}.list(params), ${propertyName}Total: ${className}.count(), audited: audited]
    }

    def create() {
		def audited = ${className}.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
        [${propertyName}: ${domainClass.propertyName}Service.create(params), audited: audited]
    }

    def save() {
		def ${propertyName}
		try{
			${propertyName} = ${domainClass.propertyName}Service.save(params)
		} catch (GenericSaveException gsx){
			render(view: "create", model: [${propertyName}: ${propertyName}])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), ${propertyName}.id])
		redirect(action: "show", id: ${propertyName}.id)
    }

    def show(${idType} id) {
		def ${propertyName} = ${className}.get(id)
		if (!${propertyName}) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), id])
			redirect(action: "list")
			return
		}
		def audited = ${className}.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[${propertyName}: ${propertyName}, audited: audited]
    }

    def edit(${idType} id) {
		def ${propertyName}
		try{
			${propertyName} = ${domainClass.propertyName}Service.edit(id)
		} catch (GenericSaveException gsx){
			flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), id])
			redirect(action: "list")
			return
		}
		def audited = ${className}.class.isAnnotationPresent( org.hibernate.envers.Audited.class )
		[${propertyName}: ${propertyName}, audited: audited]
    }

    def update(${idType} id, Long version) {
		def ${propertyName}
		try{
			${propertyName} = ${domainClass.propertyName}Service.update(id, version, params)
		} catch (GenericSaveException gsx) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), id])
			redirect(action: "list")
			return
		} catch (GenericVersionMismatchException gsx) {
			${propertyName}.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: '${domainClass.propertyName}.label', default: '${className}')] as Object[],
						"Another user has updated this ${className} while you were editing")
			  render(view: "edit", model: [${propertyName}: ${propertyName}])
			  return
		} catch (GenericSaveException gsx){
			render(view: "edit", model: [${propertyName}: ${propertyName}])
			return
		}
		flash.message = message(code: 'default.updated.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), ${propertyName}.id])
		redirect(action: "show", id: ${propertyName}.id)
    }

    def delete(${idType} id) {
		try{
			def ${propertyName} = ${domainClass.propertyName}Service.delete(id)
			flash.message = message(code: 'default.deleted.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), id])
			redirect(action: "list")
		} catch (GenericNotFoundException e) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), id])
			redirect(action: "list")
			return
		} catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), id])
			redirect(action: "show", id: id)
			return
		}		
    }
	
	def handleDuplicateKeyException(DuplicateKeyException dkx) {
		def ${propertyName} = new ${className}(params)
		log.error "DuplicateKeyException \${params.keyName}"
		flash.message = message(code: 'default.duplicate.message', args: [message(code: '${domainClass.propertyName}.label', default: '${className}'), ${propertyName}.id])
		render(view: "create", model: [${propertyName}: ${propertyName}])
		return
	}
}
